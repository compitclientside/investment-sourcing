# investment-sourcing

This project handles all investments related logic:

* Creating deals (jobs folder)
* Viewing deals (all serverlesss related code)

## Usage

### Running locally

```sh
$ npm i -g serverless # Install serverless globally
$ npm i # install the project locally
$ sls offline
```

### Debugging
To debug, either run:
```sh
$ npm run debug
```
and then attach the process with vscode (usually port 9229)

OR use the following vscode config:
```json
{
  "type": "node",
  "request": "launch",
  "name": "Debug Serverless Offline",
  "program": "${workspaceFolder}/node_modules/.bin/serverless",
  "outputCapture": "std",
  "skipFiles": [
    "<node_internals>/**"
  ],
  "args": [
    "offline",
    "--noTimeout"
  ]
}
```
This would be the same as running:
```sh
$ node --inspect ./node_modules/.bin/serverless offline --noTimeout
```


## Deploying
