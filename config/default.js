module.exports = {
  propertyDb: 'property',
  investmentsDb: '',
  aws: {
    redshift: {
      user: process.env.AWS_REDSHIFT_USER,
      password: process.env.AWS_REDSHIFT_PASS,
      host: process.env.AWS_REDSHIFT_HOST,
      port: process.env.AWS_REDSHIFT_PORT,
      database: process.env.AWS_REDSHIFT_DB
    },
    firehose: {
      deliveryStream: 'investments-sourcing',
      region: 'us-east-1'
    }
  },
  paging: {
    size: 10
  },
  defaultStaticFields: {
    purchase_price_discount: 0.05,
    sale_price_discount: 0,
    renovation_rate: 0.12,
    closing_costs_rate: 0.03,
    bank_mortgage_fee_rate: 0.005,
    compit_acquisition_fee_rate: 0.01,
    maintenance_fee_rate: 0.02,
    mortgage_rate: 0.045,
    asset_mgmt_fee_rate: 0.01,
    loan_to_value_rate: 0.7, 
    sale_closing_costs_rate: 0.02,
    sale_seller_broker_fee_rate: 0.025,
    compit_seller_rate: 0.25,
    sale_buyer_broker_fee_rate: 0.025,
    compit_sell_out_fee_rate: 0.01
  },
  logger: {
    logentriesToken: '68b5ca9f-b01e-4952-a6f0-7a4678b041ad' // dev investments
  },
};
