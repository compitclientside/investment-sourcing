const config = require('config');
const moment = require('moment');
const SubjectService = require('./services/Subject.service');
const BedBathService = require('./services/BedBath.service');
const MediaService = require('./services/Media.service');
const CompService = require('./services/Comp.service');
const IndexService = require('./services/Index.service');
const ContextComponent = require('./components/Context.component');
const IndexComponent = require('./components/Index.component');
const SubjectTransformerComponent = require('./components/SubjectTransformer.component');
const firehoseClient = require('./dal/clients/firehose.client');
const logger = require('./common/logger');

class Investments {
  constructor(db) {
    this.container = {
      subjectService: null,
      bedBathService: null,
      mediaService: null,
      compService: null,
      indexService: null,
      SubjectTransformer: null
    };
    this.db = db;
  }
  /**
   * Main initialization,
   * adding all main services to the Investment's container
   * to be accessible for all services
   *
   * @returns {Promise<void>}
   * @memberof Investments
   */
  async initialize() {
    try {
      this.container.subjectService = new SubjectService(this.db, this);
      this.container.bedBathService = new BedBathService();
      this.container.mediaService = new MediaService(this.db, this);
      this.container.compService = new CompService(this.db, this);
      this.container.indexService = new IndexService(this.db, this);
      this.firehose = new firehoseClient(config.aws.firehose);
    }
    catch (err) {
      logger.error('Investments::initialize error:', { err });
    }
  }
  /**
   * Shutdown all resources
   *
   * @returns {Promise<void>}
   * @memberof Investments
   */
  async end() { }
  /**
   * Shutdown all resources
   *
   * @returns {Promise<void>}
   * @memberof Investments
   */
  async clear() {
    return await this.container.subjectService.clear();
  }
  /**
   * Fetching all raw subjects to be transformed
   * and saved into the 'investments' table
   *
   * @returns {Promise<any>}
   * @memberof Investments
   */
  async getRawSubjects() {
    return await this.container.subjectService.getRawSubjects();
  }
  /**
   * Doing a single subject transformation on the fly using optional custom params
   *
   * @param {string} entityId
   * @param {object} params
   * @returns
   * @memberof Investments
   */
  async generate(entityId, params) {
    return await this.container.subjectService.generate(entityId, params);
  }
  /**
   * Fetching the transformed subjects from the 'investments' table
   * This function returns a list of subjects without the additional comps property
   *
   * @param {number} page
   * @param {number} size
   * @returns {Promise<any>}
   * @memberof Investments
   */
  async getCalculatedSubjects(page, size) {
    if (isNaN(page) || isNaN(size) || page < 0 || size < 1)
      throw new Error('Invalid params');
    if (page !== 0)
      page = page - 1; // The code is zero based.
    return await this.container.subjectService.getCalculatedSubjects(page, size);
  }
  /**
   * Fetching a single transformed subject with all its comps data
   *
   * @param {String} entityId
   * @returns {Promise<any>}
   * @memberof Investments
   */
  async getCalculatedSubjectByEntityId(entityId, upperBeds, upperBaths) {
    return await this.container.subjectService.getCalculatedSubjectByEntityId(entityId, upperBeds, upperBaths);
  }
  /**
   * Fetching all comps in a friendly format,
   * all properties are in camel case, easy to consume by a client app
   *
   * @param {string} regionName
   * @param {number} beds
   * @param {number} baths
   * @param {number} ngId
   * @param {number} blId
   * @returns
   * @memberof Investments
   */
  async getCompsBeautify(regionName, beds, baths, ngId, blId) {
    return await this.container.compService.getCompsBeautify(regionName, beds, baths, ngId, blId);
  }
  /**
   * Fetching all subject comps data and constructing a new Context object
   * which holds all comps splitted to region, neighborhood and builiding
   * in lists of 1 and 2 years
   *
   * @param {any} subject
   * @param {any} beds
   * @param {any} baths
   * @returns
   * @memberof Investments
   */
  async getContextForSubject(subject, upperBeds, upperBaths) {
    const { region, neighborhood, building } = await this.container.compService.getCachedCompsForSubject(subject.region, subject.neighborhoods_group, subject.building_id);
    return new ContextComponent(region, neighborhood, building, upperBeds, upperBaths);
  }
  async getIndexForSubject(subject, upperBeds, upperBaths) {
    // Passing here the upperBeds upperBaths will give us indexes that
    // have beds === upperBeds & baths === (upperBaths | upperBaths - 0.5)
    const { region, neighborhood, building } = await this.container.indexService.getCachedIndexesForSubject(subject.region, subject.neighborhoods_group, subject.building_id, upperBeds, upperBaths);
    return new IndexComponent(subject, region, neighborhood, building, upperBeds, upperBaths);
  }
  /**
   *
   *
   * @param {number} cycleId
   * @returns
   * @memberof Investments
   */
  async getMediaByCycleId(cycleId) {
    return await this.container.mediaService.getByCycleId(cycleId);
  }
  /**
   * Simple service to get the optimized upper beds/baths for a subject
   *
   * @param {number} beds
   * @param {number} baths
   * @param {number} sqft
   * @returns {IBedsBathsPair[]}
   * @memberof Investments
   */
  getUpperBedsBaths(beds, baths) {
    return this.container.bedBathService.getUpperBedsBaths(beds, baths);
  }
  /**
   * Load data to redshift to the 'investments' table
   *
   * @param {any[]} buffer
   * @memberof Investments
   */
  async load(buffer) {
    await this.firehose.pushMany(buffer);
  }
  /**
   *
   *
   * @param {any} subject
   * @param {any} context
   * @param {any} beds
   * @param {any} baths
   * @param {any} cfg
   * @returns
   * @memberof Investments
   */
  getSubjectTransformer(subject, context, index, beds, baths, cfg) {
    return new SubjectTransformerComponent(subject, context, index, beds, baths, cfg, this);
  }
  /**
   * Filter fetched raw subjects before start the
   * transformation process
   *
   * @param {any} subjects
   * @returns
   * @memberof Investments
   */
  filter(subjects) {
    return subjects.filter(subject => {
      return Investments.isValidStatus(subject)
    });
  }
  /**
   * Status validation
   *
   * @static
   * @param {any} subject
   * @returns
   * @memberof Investments
   */
  static isValidStatus(subject) {
    if (['active', 'pending'].includes(subject.status))
      return true;
    if (['perm off market', 'temp off market'].includes(subject.status) && moment().diff(moment(subject.action_date), 'days') < 547)
      return true;
    if ('sold' === subject.status && moment().diff(moment(subject.action_date), 'years', true) <= 4)
      return true;
    return false;
  }

}
Investments.CHUNK_SIZE_TO_LOAD = 25;
module.exports = Investments;
