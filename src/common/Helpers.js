const snake = require('to-snake-case');
const _ = require('lodash');
class Helpers {
    static getActualPurchasePrice(purchasePriceDiscount, effPrice) {
        const args = Array.from(arguments);
        if (!Helpers.areAllArgsValidNumbers(args))
            return null;
        return (1 - purchasePriceDiscount) * effPrice;
    }
    static getActualSalePrice(salePriceDiscount, targetSalePrice) {
        const args = Array.from(arguments);
        if (!Helpers.areAllArgsValidNumbers(args))
            return null;
        return (1 - salePriceDiscount) * targetSalePrice;
    }
    static getTotalAcquisitionCost(loanToValueRate, bankMortgageFeeRate, actualPurchasePrice, closingCostsRate, compitAcquisitionFeeRate, renovationRate, effPrice) {
        const args = Array.from(arguments);
        if (!Helpers.areAllArgsValidNumbers(args))
            return null;
        return (1 + (loanToValueRate * bankMortgageFeeRate) / (1 - loanToValueRate * bankMortgageFeeRate)) *
            (actualPurchasePrice * (1 + closingCostsRate + compitAcquisitionFeeRate) + (renovationRate * effPrice));
    }
    static areAllArgsValidNumbers(argumentsArr) {
        let isValid = true;
        for (const arg of argumentsArr) {
            if (!_.isNumber(arg)) {
                isValid = false;
                break;
            }
        }
        return isValid;
    }
    // TODO: temp solution should be handle in the db level!
    static castToNumber(objs) {
        if (!_.isArray(objs))
            objs = [objs];
        objs.forEach(comp => {
            for (const prop of Object.keys(comp)) {
                if (!['latitude', 'longitude', 'line', 'unit'].includes(prop)) {
                    if (/^-?\d+(\.\d+)?$/.test(comp[prop])) {
                        comp[prop] = parseFloat(comp[prop]);
                    }
                }
            }
        });
        return objs;
    }
    static toSnakeCase(obj) {
        for (const prop of Object.keys(obj)) {
            obj[snake(prop)] = obj[prop];
            delete obj[prop];
        }
        return obj;
    }
}
module.exports = Helpers;
