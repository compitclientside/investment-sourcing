

const _ = require('lodash');
const stats = require('stats-lite');
class Stats {
    static getAvgForProperty(propertyOrFunc, items) {
        let total = 0;
        let itemsWithValue = 0;
        for (let i = 0; i < items.length; i++) {
            const item = items[i];
            const currentValue = _.isFunction(propertyOrFunc) ? propertyOrFunc(item) : item[propertyOrFunc];
            if (_.isNumber(currentValue)) {
                total += currentValue;
                itemsWithValue++;
            }
        }
        if (!itemsWithValue)
            return null;
        return total / itemsWithValue;
    }
    static getMedianForProperty(propertyOrFunc, items) {
        const values = [];
        for (const item of items) {
            const currentValue = _.isFunction(propertyOrFunc) ? propertyOrFunc(item) : item[propertyOrFunc];
            if (currentValue)
                values.push(currentValue);
        }
        if (!values.length)
            return null;
        return stats.median(values);
    }
    static deviation(value1, value2) {
        if (!_.isNumber(value1) || !_.isNumber(value2)) {
            return null;
        }
        return 1 - (value1 / value2);
    }
    static getActPendSoldCount(comps) {
        const countObj = { active: 0, pending: 0, sold: 0 };
        for (const comp of comps) {
            if (comp.status in countObj) {
                countObj[comp.status]++;
            }
        }
        return countObj;
    }
    static getActiveMinMaxForProperty(property, comps) {
        const actives = comps.filter(comp => comp.status === 'active');
        const values = actives.map(comp => comp[property]);
        const min = _.min(values);
        const max = _.max(values);
        return { min, max };
    }
}
module.exports = Stats;
