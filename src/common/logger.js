

const config = require('config');
const LogEntries = require('le_node');
const bunyan = require('bunyan');
class Logger {
    constructor() {
        const logEntries = LogEntries.bunyanStream({
            token: config.logger.logentriesToken
        });
        this.logger = bunyan.createLogger({
            name: 'investment',
            level: 'debug',
            streams: [
                logEntries,
                { stream: process.stdout }
            ]
        });
    }
    log(message = '', obj = {}) {
        this.logger.info(obj, message);
    }
    info(message = '', obj = {}) {
        this.logger.info(obj, message);
    }
    debug(message = '', obj = {}) {
        this.logger.debug(obj, message);
    }
    warn(message = '', obj = {}) {
        this.logger.warn(obj, message);
    }
    error(message = '', obj = {}) {
        this.logger.error(obj, message);
    }
}
module.exports = new Logger();
