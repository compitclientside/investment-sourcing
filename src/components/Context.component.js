

const moment = require('moment');
class Context {
    constructor(region, neighborhood, building, upperBeds, upperBaths) {
        this.regionComps2Year = Context.rgNgFilter(region, upperBeds, upperBaths);
        this.neighborhoodComps2Year = Context.rgNgFilter(neighborhood, upperBeds, upperBaths);
        this.buildingComps2Year = Context.bldgFilter(building, upperBeds, upperBaths);
        this.regionComps1Year = Context.comps1YearFilter(this.regionComps2Year);
        this.neighborhoodComps1Year = Context.comps1YearFilter(this.neighborhoodComps2Year);
        this.buildingComps1Year = Context.comps1YearFilter(this.buildingComps2Year);
    }
    static comps1YearFilter(comps) {
        return comps.filter(comp => moment().diff(moment(comp.last_action_date), 'years', true) <= 1);
    }
    static bldgFilter(comps, beds, baths) {
        return comps.filter(comp => {
            return !isNaN(comp.beds) && !isNaN(comp.baths) && comp.beds === beds && comp.baths >= (baths - 0.5) && comp.baths <= (baths + 1);
        });
    }
    static rgNgFilter(comps, beds, baths) {
        return comps.filter(comp => {
            return !isNaN(comp.beds) && !isNaN(comp.baths) && comp.beds === beds && comp.baths >= baths;
        });
    }
}
module.exports = Context;
