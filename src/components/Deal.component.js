

const _ = require('lodash');
const Stats = require('../common/Stats');
const Helpers = require('../common/Helpers');
class Deal {
    constructor(term, output) {
        this.term = term;
        this.output = output;
        this.roe_waterfall = [];
    }
    calculate() {
        this.totalHoldingCosts();
        this.totalDealCosts();
        this.capRateOnEquity();
        this.maintenanceFeePrice();
        this.assetMgmtFeePrice();
        this.mortgagePrice();
        this.mortgageAmountHoldingCost();
        this.totalMortgageAmount();
        this.totalEquityRequired();
        this.grossProfit();
        this.returnOnEquity();
        this.returnOnInvestment();
        this._roe();
        this._roe_rank();
        this._roi();
        return this;
    }
    totalHoldingCosts() {
        this.total_holding_costs = this.getTotalHoldingCosts(this.term, this.output.actual_purchase_price, this.output.maintenance_fee_rate, this.output.total_acquisition_cost, this.output.loan_to_value_rate, this.output.mortgage_rate, this.output.asset_mgmt_fee_rate);
    }
    totalDealCosts() {
        if (_.isNumber(this.output.total_acquisition_cost) && _.isNumber(this.total_holding_costs)) {
            this.total_deal_costs = this.output.total_acquisition_cost + this.total_holding_costs;
        }
        else {
            this.total_deal_costs = null;
        }
    }
    // TODO: update filed
    capRateOnEquity() {
        if (_.isNumber(this.output.monthly_mortgage_payment) && _.isNumber(this.output.upper_bed_indexed_rental) && _.isNumber(this.total_holding_costs) && _.isNumber(this.output.total_acquisition_cost)) {
            this.cap_rate_on_equity = 12 * (this.output.upper_bed_indexed_rental - this.output.monthlies - this.output.monthly_mortgage_payment) /
                (this.total_holding_costs + (1 - this.output.loan_to_value_rate) * this.output.total_acquisition_cost);
        }
        else {
            this.cap_rate_on_equity = null;
        }
    }
    maintenanceFeePrice() {
        this.maintenance_fee_price = 1 / 12 * this.term * (this.output.maintenance_fee_rate * this.output.actual_purchase_price);
    }
    assetMgmtFeePrice() {
        this.asset_mgmt_fee_price = 1 / 12 * this.term * (this.output.actual_purchase_price * this.output.asset_mgmt_fee_rate);
    }
    mortgagePrice() {
        this.mortgage_price = 1 / 12 * this.term * (this.output.total_acquisition_cost * this.output.mortgage_rate * this.output.loan_to_value_rate);
    }
    mortgageAmountHoldingCost() {
        this.mortgage_amount_holding_cost = this.output.loan_to_value_rate * (this.maintenance_fee_price + this.asset_mgmt_fee_price +
            this.output.mortgage_rate * (this.output.mortgage_amount_acquisition_cost + this.maintenance_fee_price + this.asset_mgmt_fee_price));
    }
    totalMortgageAmount() {
        this.total_mortgage_amount = this.output.mortgage_amount_acquisition_cost + this.mortgage_amount_holding_cost;
    }
    totalEquityRequired() {
        this.total_equity_required = this.total_deal_costs - this.total_mortgage_amount;
    }
    // actualSalePrice, totalDealCosts, totalSaleCosts
    grossProfit() {
        this.gross_profit = this.calculateGrossProfit(this.output.purchase_price_discount, this.output.sale_price_discount);
        this.gross_profit_rate = this.gross_profit / (this.total_deal_costs + this.output.total_sales_costs);
        this.gross_profit_005_0 = this.calculateGrossProfit(0.05, 0);
        this.gross_profit_0075_0 = this.calculateGrossProfit(0.075, 0);
        this.gross_profit_01_0 = this.calculateGrossProfit(0.1, 0);
        this.gross_profit_0_005 = this.calculateGrossProfit(0, 0.05);
        this.gross_profit_005_005 = this.calculateGrossProfit(0.05, 0.05);
        this.gross_profit_0075_005 = this.calculateGrossProfit(0.075, 0.05);
        this.gross_profit_01_005 = this.calculateGrossProfit(0.1, 0.05);
        this.gross_profit_0_01 = this.calculateGrossProfit(0, 0.1);
        this.gross_profit_005_01 = this.calculateGrossProfit(0.05, 0.1);
        this.gross_profit_0075_01 = this.calculateGrossProfit(0.075, 0.1);
        this.gross_profit_01_01 = this.calculateGrossProfit(0.1, 0.1);
    }
    returnOnEquity() {
        this.return_on_equity = this.gross_profit / ((1 - this.output.loan_to_value_rate) * this.output.total_acquisition_cost + this.total_holding_costs);
    }
    returnOnInvestment() {
        this.return_on_investment = this.gross_profit / (this.total_deal_costs + this.output.total_sales_costs);
    }
    _roe() {
        this.roe_waterfall.push(this.roe = this.calculateROE(this.output.purchase_price_discount, this.output.sale_price_discount));
        this.roe_waterfall.push(this.roe_005_0 = this.calculateROE(0.05, 0));
        this.roe_waterfall.push(this.roe_0075_0 = this.calculateROE(0.075, 0));
        this.roe_waterfall.push(this.roe_01_0 = this.calculateROE(0.1, 0));
        this.roe_waterfall.push(this.roe_0_005 = this.calculateROE(0, 0.05));
        this.roe_waterfall.push(this.roe_005_005 = this.calculateROE(0.05, 0.05));
        this.roe_waterfall.push(this.roe_0075_005 = this.calculateROE(0.075, 0.05));
        this.roe_waterfall.push(this.roe_01_005 = this.calculateROE(0.1, 0.05));
        this.roe_waterfall.push(this.roe_0_01 = this.calculateROE(0, 0.1));
        this.roe_waterfall.push(this.roe_005_01 = this.calculateROE(0.05, 0.1));
        this.roe_waterfall.push(this.roe_0075_01 = this.calculateROE(0.075, 0.1));
        this.roe_waterfall.push(this.roe_01_01 = this.calculateROE(0.1, 0.1));
    }
    _roe_rank() {
        this.roe_waterfall = this.roe_waterfall.map(roe => {
            return { roe };
        });
        this.roe_rank = Stats.getAvgForProperty('roe', this.roe_waterfall);
    }
    _roi() {
        this.roi = this.gross_profit / (this.total_deal_costs + this.output.total_sales_costs);
    }
    calculateROE(purchasePriceDiscount, salePriceDiscount) {
        // -- Steps for calculating grossProfit
        const actualSalePrice = Helpers.getActualSalePrice(salePriceDiscount, this.output.target_sale_price);
        const grossProfit = this.getGrossProfit(actualSalePrice, this.total_deal_costs, this.output.total_sales_costs);
        // -- Steps for calculating totalAcquisitionCost
        const actualPurchasePrice = Helpers.getActualPurchasePrice(purchasePriceDiscount, this.output.eff_price);
        const totalAcquisitionCost = Helpers.getTotalAcquisitionCost(this.output.loan_to_value_rate, this.output.bank_mortgage_fee_rate, actualPurchasePrice, this.output.closing_costs_rate, this.output.compit_acquisition_fee_rate, this.output.renovation_rate, this.output.eff_price);
        // -- Steps for calculating totalHoldingCosts
        const totalHoldingCosts = this.getTotalHoldingCosts(this.term, actualPurchasePrice, this.output.maintenance_fee_rate, totalAcquisitionCost, this.output.loan_to_value_rate, this.output.mortgage_rate, this.output.asset_mgmt_fee_rate);
        return grossProfit / (totalHoldingCosts + (1 - this.output.loan_to_value_rate) * totalAcquisitionCost);
    }
    calculateGrossProfit(purchasePriceDiscount, salePriceDiscount) {
        // -- Steps for calculating actual_sale_price.
        const actualSalePrice = Helpers.getActualSalePrice(salePriceDiscount, this.output.target_sale_price);
        // -- Steps for calculating total_deal_costs.
        const actualPurchasePrice = Helpers.getActualPurchasePrice(purchasePriceDiscount, this.output.eff_price);
        const totalAcquisitionCost = Helpers.getTotalAcquisitionCost(this.output.loan_to_value_rate, this.output.bank_mortgage_fee_rate, actualPurchasePrice, this.output.closing_costs_rate, this.output.compit_acquisition_fee_rate, this.output.renovation_rate, this.output.eff_price);
        const totalHoldingCosts = this.getTotalHoldingCosts(this.term, actualPurchasePrice, this.output.maintenance_fee_rate, totalAcquisitionCost, this.output.loan_to_value_rate, this.output.mortgage_rate, this.output.asset_mgmt_fee_rate);
        const totalDealCosts = totalAcquisitionCost + totalHoldingCosts;
        return this.getGrossProfit(actualSalePrice, totalDealCosts, this.output.total_sales_costs);
    }
    getGrossProfit(actualSalePrice, totalDealCosts, totalSaleCosts) {
        if (!Helpers.areAllArgsValidNumbers(Array.from(arguments)))
            return null;
        return actualSalePrice - (totalDealCosts + totalSaleCosts);
    }
    getTotalHoldingCosts(term, actualPurchasePrice, maintenanceFeeRate, totalAcquisitionCost, loanToValueRate, mortgageRate, assetMgmFeeRate) {
        if (!Helpers.areAllArgsValidNumbers(Array.from(arguments)))
            return null;
        return 1 / 12 * term * (actualPurchasePrice * maintenanceFeeRate +
            totalAcquisitionCost * loanToValueRate * mortgageRate) +
            actualPurchasePrice * assetMgmFeeRate;
    }
}
module.exports = Deal;
