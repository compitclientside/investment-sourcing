

const _ = require('lodash');
const Stats = require('../common/Stats');
class Index {
    constructor(subject, region, neighborhood, building, upperBeds, upperBaths) {
        this.subject = subject;
        this.region = region;
        this.neighborhood = neighborhood;
        this.building = building;
        this.upperBeds = upperBeds;
        this.upperBaths = upperBaths;
    }
    getUpperBedIndexedRent() {
        let results;
        // If the subject doesnt have a rent approx value,
        // there is nothing we can do here.
        if (!_.isNumber(this.subject.rent_approx)) {
            return null;
        }
        // Building level.
        if (!_.isEmpty(this.building)) {
            results = this.building.filter(index => {
                return index.beds === this.upperBeds &&
                    index.baths === this.upperBaths &&
                    index.approx >= this.subject.rent_approx;
            });
            if (!_.isEmpty(results)) {
                return Stats.getMedianForProperty('approx', results);
            }
        }
        // Neighborhood level.
        if (!_.isEmpty(this.neighborhood)) {
            // The next steps relies on the subject having a size value,
            // if it doesnt have, we can skip these steps.
            if (_.isNumber(this.subject.size)) {
                const min = 0.8 * this.subject.size;
                const max = 1.2 * this.subject.size;
                results = this.neighborhood.filter(index => {
                    return index.beds === this.upperBeds &&
                        index.baths === this.upperBaths &&
                        Index.isSizeInRange(index.size, min, max) &&
                        index.approx >= this.subject.rent_approx;
                });
                if (!_.isEmpty(results)) {
                    return Stats.getMedianForProperty('approx', results);
                }
                results = this.neighborhood.filter(index => {
                    return index.beds === this.upperBeds &&
                        index.baths >= this.upperBaths - 0.5 &&
                        Index.isSizeInRange(index.size, min, max) &&
                        index.approx >= this.subject.rent_approx;
                });
                if (!_.isEmpty(results)) {
                    return Stats.getMedianForProperty('approx', results);
                }
            }
            results = this.neighborhood.filter(index => {
                return index.beds === this.upperBeds &&
                    index.baths === this.upperBaths &&
                    index.approx >= this.subject.rent_approx;
            });
            if (!_.isEmpty(results)) {
                return Stats.getMedianForProperty('approx', results);
            }
            results = this.neighborhood.filter(index => {
                return index.beds === this.upperBeds &&
                    index.baths >= this.upperBaths - 0.5 &&
                    index.approx >= this.subject.rent_approx;
            });
            if (!_.isEmpty(results)) {
                return Stats.getMedianForProperty('approx', results);
            }
        }
        // Region level.
        if (_.isEmpty(this.region)) {
            return null;
        }
        results = this.region.filter(index => {
            return index.beds === this.upperBeds &&
                index.baths >= this.upperBaths - 0.5 &&
                index.approx >= this.subject.rent_approx;
        });
        if (!_.isEmpty(results)) {
            return Stats.getMedianForProperty('approx', results);
        }
        return null;
    }
    static isSizeInRange(size, min, max) {
        return size >= min && size <= max;
    }
}
module.exports = Index;
