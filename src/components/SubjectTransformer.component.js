const moment = require('moment');
const _ = require('lodash');
const Stats = require('../common/Stats');
const DealComponent = require('./Deal.component');
const Helpers = require('../common/Helpers');
class SubjectTransformer {
    constructor(subject, context, index, newBeds, newBaths, cfg, parent) {
        this.subject = subject;
        this.context = context;
        this.index = index;
        this.newBeds = newBeds;
        this.newBaths = newBaths;
        this.parent = parent;
        this.output = Object.assign({}, cfg);
    }
    async transform() {
        try {
            await this.setOutputValues();
            this.setOutputBuildingCompsValues();
            this.setOutputNeighborhoodCompsValues();
            this.setOutputRegionCompsValues();
            this.setDeals();
        }
        catch (err) {
            throw new Error(`SubjectTransformer::transform, entityId: ${this.subject.entity_id}, errMsg: ${err.message}`);
        }
        return this.output;
    }
    async setOutputValues() {
        this.setIds();
        this.address();
        this.unit();
        this.entityId();
        await this.getCalculatedSubjectLastClosingActions();
        this.bedsBaths();
        await this.getCalculatedSubjectLastEqualBedSubject();
        this.sqft();
        this.status();
        this.dom();
        this.date();
        this.price();
        this.discountPct();
        this.ppsf();
        this.region();
        this.latLong();
        this.indexedPrice();
        this.approxPrice();
        this.effPrice();
        this.effPriceDev();
        this.indexedPpsf();
        this.actualPurchasePrice();
        this.renovationCost();
        this.targetSalePrice();
        this.actualSalePrice();
        this.actualSalePpsf();
        this.monthlies();
        this.convertible();
        this.neighborhoodsGroup();
        this.tenantInPlace();
        this.renovationPrice();
        this.capRate();
        this.capRateUpperBed();
        this.capRatePostOptimization();
        this.quantityInLine();
        this.url();
        this.ownership();
        this.developmentStatus();
        this.description();
        this.media();
        this.closingCostsPrice();
        this.compitAcquisitionFeePrice();
        this.bankMortgageFeePrice();
        this.saleClosingCostsPrice();
        this.saleSellerBrokerFeePrice();
        this.saleBuyerBrokerFeePrice();
        this.compitSellOutFeePrice();
        this.link();
        this.output.created_at = new Date();
    }
    setIds() {
        this.output.neighborhood_id = this.subject.neighborhood_id;
        this.output.building_id = this.subject.building_id;
    }
    address() {
        this.output.address = this.subject.address;
    }
    unit() {
        this.output.unit = this.subject.unit;
    }
    entityId() {
        this.output.entity_id = this.subject.entity_id;
        this.output.listing_entity_id = this.subject.listing_entity_id;
    }
    // // TODO: check for silent/ use Itamar query
    async getCalculatedSubjectLastClosingActions() {
        const { lastSoldAction, lastRentedAction } = await this.parent.container.subjectService.getCalculatedSubjectLastClosingActions(this.subject.apartment_id);
        // Sold Action.
        this.output.last_subject_sale_date = lastSoldAction ? lastSoldAction.action_date : null;
        this.output.last_subject_sale_price = lastSoldAction ? lastSoldAction.price : null;
        this.output.last_subject_sale_status = lastSoldAction ? lastSoldAction.status : null;
        this.output.last_subject_sale_dom = lastSoldAction ? lastSoldAction.days_on_market : null;
        this.output.last_subject_sale_discount_pct = lastSoldAction ? lastSoldAction.price_change_pct : null;
        // Rent Action.
        this.output.last_subject_rent_date = lastRentedAction ? lastRentedAction.action_date : null;
        this.output.last_subject_rent_price = lastRentedAction ? lastRentedAction.price : null;
        this.output.last_subject_rent_status = lastRentedAction ? lastRentedAction.status : null;
        this.output.last_subject_rent_dom = lastRentedAction ? lastRentedAction.days_on_market : null;
        this.output.last_subject_rent_discount_pct = lastRentedAction ? lastRentedAction.price_change_pct : null;
    }
    bedsBaths() {
        const beds = parseFloat(this.subject.beds);
        const baths = parseFloat(this.subject.baths);
        this.output.beds = isNaN(beds) ? null : beds;
        this.output.baths = isNaN(baths) ? null : baths;
        this.output.new_beds = this.newBeds;
        this.output.new_baths = this.newBaths;
    }
    async getCalculatedSubjectLastEqualBedSubject() {
        const response = await this.parent.container.subjectService.getCalculatedSubjectLastEqualBedSubject(this.output.beds, this.output.baths, this.subject.apartment_id, this.subject.building_id, this.subject.line);
        this.output.last_equal_bed_date = response ? response.action_date : null;
        this.output.last_equal_bed_status = response ? response.status : null;
        this.output.last_equal_bed_price = response ? response.price : null;
        this.output.last_equal_bed_unit = response ? response.unit : null;
    }
    sqft() {
        const size = parseFloat(this.subject.size);
        this.output.sqft = isNaN(size) ? null : size;
    }
    status() {
        this.output.status = this.subject.status;
    }
    dom() {
        if (this.subject.status === 'active') {
            this.output.dom = moment().diff(moment(this.subject.effective_date), 'days');
        }
        else {
            this.output.dom = this.subject.days_on_market;
        }
    }
    date() {
        this.output.date = this.subject.effective_date;
    }
    price() {
        this.output.price = this.subject.price;
    }
    discountPct() {
        this.output.discount_pct = this.subject.discount_pct;
    }
    ppsf() {
        this.output.ppsf = this.output.sqft ? (this.output.price / this.output.sqft) : null;
    }
    // TODO: move to region seciton.
    region() {
        this.output.region = this.subject.region;
    }
    latLong() {
        this.output.longitude = this.subject.longitude;
        this.output.latitude = this.subject.latitude;
    }
    indexedPrice() {
        const indexedPrice = parseFloat(this.subject.indexed_price);
        if (!isNaN(indexedPrice)) {
            this.output.indexed_price = indexedPrice * this.output.price;
            this.output.indexed_price_dev = Stats.deviation(this.output.price, this.output.indexed_price);
        }
        else {
            this.output.indexed_price = this.output.indexed_price_dev = null;
        }
    }
    approxPrice() {
        this.output.sale_approx = null;
        if (this.subject.sale_approx && /^(price|pricePerSize)$/.test(this.subject.sale_sought_after_property)) {
            if (this.subject.sale_sought_after_property === 'price') {
                this.output.sale_approx = this.subject.sale_approx;
            }
            else if (this.subject.size) {
                this.output.sale_approx = this.subject.sale_approx * this.subject.size;
            }
        }
        this.output.sale_approx_dev = Stats.deviation(this.subject.price, this.output.sale_approx);
        this.output.rent_approx = this.output.indexed_rental_as_is = this.subject.rent_approx || null;
        this.output.rent_approx_dev = Stats.deviation(this.subject.price, this.output.rent_approx);
        this.output.upper_bed_indexed_rental = this.index.getUpperBedIndexedRent();
    }
    effPrice() {
        if (this.output.status === 'active') {
            this.output.eff_price = this.output.price;
        }
        else if (_.isNumber(this.output.indexed_price) && _.isNumber(this.output.sale_approx)) {
            this.output.eff_price = (this.output.indexed_price + this.output.sale_approx) / 2;
        }
        else {
            this.output.eff_price = null;
        }
    }
    effPriceDev() {
        if (this.output.status === 'active') {
            this.output.eff_price_dev = Stats.deviation(this.output.eff_price, this.output.price);
        }
        else {
            this.output.eff_price_dev = null;
        }
    }
    indexedPpsf() {
        if (_.isNumber(this.output.indexed_price) && _.isNumber(this.output.sqft)) {
            this.output.indexed_ppsf = this.output.indexed_price / this.output.sqft;
        }
        else {
            this.output.indexed_ppsf = null;
        }
    }
    actualPurchasePrice() {
        if (!this.output.actual_purchase_price) {
            this.output.actual_purchase_price = Helpers.getActualPurchasePrice(this.output.purchase_price_discount, this.output.eff_price);
        }
        else {
            // The 'actual_purchase_price' is not calculated it was given to us as a fix value.
            // So, we need to figure out what it the corresponding 'eff_price' value.
            this.output.eff_price = this.output.actual_purchase_price / (1 - this.output.purchase_price_discount);
            // TODO: this line is only valid wheb status is Active.
            this.output.eff_price_dev = Stats.deviation(this.output.eff_price, this.output.eff_price);
        }
    }
    renovationCost() {
        this.output.renovation_cost = this.output.actual_purchase_price * this.output.renovation_rate;
    }
    targetSalePrice() {
        // Check if we already got it as an editable field.
        if (!this.output.target_sale_price) {
            const neighborhood_upper_bed_avg_price = Stats.getAvgForProperty('price', this.context.neighborhoodComps1Year);
            const building_upper_bed_avg_price = Stats.getAvgForProperty('price', this.context.buildingComps1Year);
            if (_.isNumber(this.output.eff_price)) {
                if (building_upper_bed_avg_price) {
                    this.output.target_sale_price = Math.min((1.8 * this.output.eff_price), (0.8 * building_upper_bed_avg_price));
                }
                else if (neighborhood_upper_bed_avg_price) {
                    this.output.target_sale_price = Math.min((1.8 * this.output.eff_price), (0.8 * neighborhood_upper_bed_avg_price));
                }
                else {
                    this.output.target_sale_price = null;
                }
            }
            else {
                this.output.target_sale_price = null;
            }
        }
    }
    actualSalePrice() {
        this.output.actual_sale_price = Helpers.getActualSalePrice(this.output.sale_price_discount, this.output.target_sale_price);
    }
    actualSalePpsf() {
        if (_.isNumber(this.output.actual_sale_price) && _.isNumber(this.output.sqft)) {
            this.output.actual_sale_ppsf = this.output.actual_sale_price / this.output.sqft;
        }
        else {
            this.output.actual_sale_ppsf = null;
        }
    }
    monthlies() {
        this.output.maintenance = this.subject.maintenance;
        this.output.tax = this.subject.tax;
        if (_.isNumber(this.subject.maintenance) && _.isNumber(this.subject.tax)) {
            this.output.monthlies = this.subject.maintenance + this.subject.tax;
        }
        else {
            this.output.monthlies = null;
        }
    }
    convertible() {
        this.output.convertible = this.subject.convertible;
    }
    // TODO: add to neighborhood secion
    neighborhoodsGroup() {
        this.output.neighborhoods_group = this.subject.neighborhoods_group;
    }
    tenantInPlace() {
        if (this.output.last_subject_rent_date) {
            this.output.tenant_in_place = moment().diff(moment(this.output.last_subject_rent_date), 'days') < 365 ? true : false;
        }
        else {
            this.output.tenant_in_place = null;
        }
    }
    renovationPrice() {
        if (!this.output.renovation_price) {
            this.output.renovation_price = this.output.renovation_rate * this.output.actual_purchase_price;
        }
    }
    capRate() {
        if (_.isNumber(this.output.indexed_rental_as_is) && _.isNumber(this.output.monthlies)) {
            this.output.cap_rate = 12 * (this.output.indexed_rental_as_is - this.output.monthlies) / (this.output.actual_purchase_price + (this.output.actual_purchase_price * this.output.closing_costs_rate));
        }
        else {
            this.output.cap_rate = null;
        }
    }
    capRateUpperBed() {
        if (_.isNumber(this.output.upper_bed_indexed_rental) && _.isNumber(this.output.monthlies)) {
            this.output.cap_rate_upper_bed = 12 * (this.output.upper_bed_indexed_rental - this.output.monthlies) /
                (this.output.actual_purchase_price + (this.output.actual_purchase_price * this.output.closing_costs_rate) + this.output.renovation_price);
        }
        else {
            this.output.cap_rate_upper_bed = null;
        }
    }
    capRatePostOptimization() {
        if (_.isNumber(this.output.cap_rate_upper_bed) && _.isNumber(this.output.cap_rate)) {
            this.output.cap_rate_post_optimization = this.output.cap_rate_upper_bed - this.output.cap_rate;
        }
    }
    quantityInLine() {
        if (_.isNumber(this.subject.floors)) {
            this.output.quantity_in_line = this.subject.floors - 2;
        }
        else {
            this.output.quantity_in_line = null;
        }
    }
    url() {
        this.output.url = this.subject.url;
    }
    ownership() {
        this.output.ownership = this.subject.ownership;
    }
    developmentStatus() {
        this.output.development_status = this.subject.development_status;
    }
    description() {
        this.output.description = this.subject.description;
    }
    media() {
        if (_.isArray(this.subject.media)) {
            this.output.photos = JSON.stringify(this.subject.media.filter(media => media.type === 'photo'));
            this.output.floorplans = JSON.stringify(this.subject.media.filter(media => media.type === 'floorplan'));
        }
        else {
            this.output.photos = this.output.floorplans = null;
        }
    }
    closingCostsPrice() {
        this.output.closing_costs_price = this.output.closing_costs_rate * this.output.actual_purchase_price;
    }
    compitAcquisitionFeePrice() {
        this.output.compit_acquisition_fee_price = this.output.compit_acquisition_fee_rate * this.output.actual_purchase_price;
    }
    bankMortgageFeePrice() {
        this.output.bank_mortgage_fee_price = (this.output.loan_to_value_rate * this.output.bank_mortgage_fee_rate / (1 - this.output.loan_to_value_rate * this.output.bank_mortgage_fee_rate)) *
            (this.output.actual_purchase_price + this.output.renovation_price + this.output.closing_costs_price + this.output.compit_acquisition_fee_price);
    }
    saleClosingCostsPrice() {
        this.output.sale_closing_costs_price = this.output.sale_closing_costs_rate * this.output.actual_sale_price;
    }
    saleSellerBrokerFeePrice() {
        this.output.sale_seller_broker_fee_price = this.output.sale_seller_broker_fee_rate * this.output.actual_purchase_price;
    }
    saleBuyerBrokerFeePrice() {
        this.output.sale_buyer_broker_fee_price = this.output.sale_buyer_broker_fee_rate * this.output.actual_sale_price;
    }
    compitSellOutFeePrice() {
        this.output.compit_sell_out_fee_price = this.output.compit_sell_out_fee_rate * this.output.actual_purchase_price;
    }
    link() {
        this.output.link = `http://investments.integration.compit.com/main/deals/${this.output.entity_id}/${this.output.new_beds}/${this.output.new_baths}`;
    }
    setOutputBuildingCompsValues() {
        this.output.building_upper_bed_avg_price = Stats.getAvgForProperty('price', this.context.buildingComps1Year);
        this.output.building_upper_bed_avg_price_dev = Stats.deviation(this.output.actual_sale_price, this.output.building_upper_bed_avg_price);
        this.output.building_upper_bed_median_price = Stats.getMedianForProperty('price', this.context.buildingComps1Year);
        this.output.building_upper_bed_median_price_dev = Stats.deviation(this.output.actual_sale_price, this.output.building_upper_bed_median_price);
        this.output.building_upper_bed_avg_monthlies = Stats.getAvgForProperty(comp => {
            if (_.isNumber(comp.maintenance) && _.isNumber(comp.tax)) {
                return comp.maintenance + comp.tax;
            }
            else {
                return null;
            }
        }, this.context.buildingComps1Year);
        this.output.building_upper_bed_avg_monthlies_dev = Stats.deviation(this.output.monthlies, this.output.building_upper_bed_avg_monthlies);
        this.output.building_upper_bed_avg_ppsf = Stats.getAvgForProperty(comp => {
            if (_.isNumber(comp.price) && _.isNumber(comp.size)) {
                return comp.price / comp.size;
            }
            else {
                return null;
            }
        }, this.context.buildingComps1Year);
        this.output.building_upper_bed_avg_ppsf_dev = Stats.deviation(this.output.actual_sale_ppsf, this.output.building_upper_bed_avg_ppsf);
        this.output.building_upper_bed_median_ppsf = Stats.getMedianForProperty(comp => {
            if (_.isNumber(comp.price) && _.isNumber(comp.size)) {
                return comp.price / comp.size;
            }
            else {
                return null;
            }
        }, this.context.buildingComps1Year);
        this.output.building_upper_bed_median_ppsf_dev = Stats.deviation(this.output.actual_sale_ppsf, this.output.building_upper_bed_avg_ppsf);
        this.output.building_upper_bed_avg_dom = Stats.getAvgForProperty('days_on_market', this.context.buildingComps1Year);
        this.output.building_upper_bed_avg_dom_dev = Stats.deviation(this.output.dom, this.output.building_upper_bed_avg_dom);
        const count = Stats.getActPendSoldCount(this.context.buildingComps2Year);
        this.output.building_upper_bed_2_yr_active = count.active;
        this.output.building_upper_bed_2_yr_pending = count.pending;
        this.output.building_upper_bed_2_yr_sold = count.sold;
        // TODO: seems invalid property name, check with client.
        this.output.building_upper_bed_avg_discount_pct = Stats.getAvgForProperty('discount_pct', this.context.buildingComps1Year);
        this.output.building_upper_bed_avg_price_change = Stats.getAvgForProperty('price_change', this.context.buildingComps1Year);
        this.output.building_upper_bed_median_price_change = Stats.getMedianForProperty('price_change', this.context.buildingComps1Year);
    }
    setOutputNeighborhoodCompsValues() {
        this.output.neighborhood_upper_bed_avg_price = Stats.getAvgForProperty('price', this.context.neighborhoodComps1Year);
        this.output.neighborhood_upper_bed_avg_price_dev = Stats.deviation(this.output.actual_sale_price, this.output.neighborhood_upper_bed_avg_price);
        this.output.neighborhood_upper_bed_median_price = Stats.getMedianForProperty('price', this.context.neighborhoodComps1Year);
        this.output.neighborhood_upper_bed_median_price_dev = Stats.deviation(this.output.actual_sale_price, this.output.neighborhood_upper_bed_median_price);
        const { min, max } = Stats.getActiveMinMaxForProperty('price', this.context.neighborhoodComps1Year);
        if (_.isNumber(min)) {
            this.output.neighborhood_upper_bed_active_min_price = min;
            this.output.neighborhood_upper_bed_active_min_price_dev = Stats.deviation(this.output.actual_sale_price, min);
        }
        else {
            this.output.neighborhood_upper_bed_active_min_price = this.output.neighborhood_upper_bed_active_min_price_dev = null;
        }
        if (_.isNumber(max)) {
            this.output.neighborhood_upper_bed_active_max_price = max;
            this.output.neighborhood_upper_bed_active_max_price_dev = Stats.deviation(this.output.actual_sale_price, max);
        }
        else {
            this.output.neighborhood_upper_bed_active_max_price = this.output.neighborhood_upper_bed_active_max_price_dev = null;
        }
        this.output.neighborhood_upper_bed_avg_ppsf = Stats.getAvgForProperty(comp => {
            if (_.isNumber(comp.price) && _.isNumber(comp.size)) {
                return comp.price / comp.size;
            }
            else {
                return null;
            }
        }, this.context.neighborhoodComps1Year);
        this.output.neighborhood_upper_bed_avg_ppsf_dev = Stats.deviation(this.output.actual_sale_ppsf, this.output.neighborhood_upper_bed_avg_ppsf);
        this.output.neighborhood_upper_bed_avg_monthlies = Stats.getAvgForProperty(comp => {
            if (_.isNumber(comp.maintenance) && _.isNumber(comp.maintenance)) {
                return comp.maintenance + comp.tax;
            }
            else {
                return null;
            }
        }, this.context.neighborhoodComps1Year);
        this.output.neighborhood_upper_bed_avg_monthlies_dev = Stats.deviation(this.output.monthlies, this.output.neighborhood_upper_bed_avg_monthlies);
        this.output.neighborhood_upper_bed_avg_dom = Stats.getAvgForProperty('days_on_market', this.context.neighborhoodComps1Year);
        this.output.neighborhood_upper_bed_active_median_ppsf = Stats.getMedianForProperty(comp => {
            if (comp.status === 'active' && _.isNumber(comp.price) && _.isNumber(comp.size)) {
                return comp.price / comp.size;
            }
            else {
                return null;
            }
        }, this.context.neighborhoodComps1Year);
        this.output.neighborhood_upper_bed_active_median_ppsf_dev = Stats.deviation(this.output.actual_sale_ppsf, this.output.neighborhood_upper_bed_active_median_ppsf);
        this.output.neighborhood_upper_bed_median_ppsf = Stats.getMedianForProperty(comp => {
            if (_.isNumber(comp.price) && _.isNumber(comp.size)) {
                return comp.price / comp.size;
            }
            else {
                return null;
            }
        }, this.context.neighborhoodComps1Year);
        this.output.neighborhood_upper_bed_median_ppsf_dev = Stats.deviation(this.output.actual_sale_ppsf, this.output.neighborhood_upper_bed_median_ppsf);
        this.output.neighborhood_upper_bed_median_monthlies = Stats.getMedianForProperty(comp => {
            if (_.isNumber(comp.maintenance) && _.isNumber(comp.tax)) {
                return comp.maintenance + comp.tax;
            }
            else {
                return null;
            }
        }, this.context.neighborhoodComps1Year);
        this.output.neighborhood_upper_bed_median_monthlies_dev = Stats.deviation(this.output.monthlies, this.output.neighborhood_upper_bed_median_monthlies);
        this.output.neighborhood_upper_bed_avg_sqft = Stats.getAvgForProperty('size', this.context.neighborhoodComps1Year);
        this.output.neighborhood_upper_bed_avg_sqft_dev = Stats.deviation(this.output.sqft, this.output.neighborhood_upper_bed_avg_sqft);
        const count = Stats.getActPendSoldCount(this.context.neighborhoodComps2Year);
        this.output.neighborhood_upper_bed_2_yr_active = count.active;
        this.output.neighborhood_upper_bed_2_yr_pending = count.pending;
        this.output.neighborhood_upper_bed_2_yr_sold = count.sold;
        this.output.neighborhood_upper_bed_avg_price_change = Stats.getAvgForProperty('price_change', this.context.neighborhoodComps1Year);
        this.output.neighborhood_upper_bed_median_price_change = Stats.getMedianForProperty('price_change', this.context.neighborhoodComps1Year);
    }
    setOutputRegionCompsValues() {
        this.output.region_upper_bed_avg_price = Stats.getAvgForProperty('price', this.context.regionComps1Year);
        this.output.region_upper_bed_avg_price_dev = Stats.deviation(this.output.actual_sale_price, this.output.region_upper_bed_avg_price);
        this.output.region_upper_bed_median_price = Stats.getMedianForProperty('price', this.context.regionComps1Year);
        this.output.region_upper_bed_median_price_dev = Stats.deviation(this.output.actual_sale_price, this.output.region_upper_bed_median_price);
        this.output.region_upper_bed_avg_monthlies = Stats.getAvgForProperty(comp => {
            if (_.isNumber(comp.maintenance) && _.isNumber(comp.tax)) {
                return comp.maintenance + comp.tax;
            }
            else {
                return null;
            }
        }, this.context.regionComps1Year);
        this.output.region_upper_bed_avg_monthlies_dev = Stats.deviation(this.output.monthlies, this.output.region_upper_bed_avg_monthlies);
        this.output.region_upper_bed_avg_ppsf = Stats.getAvgForProperty(comp => {
            if (_.isNumber(comp.price) && _.isNumber(comp.size)) {
                return comp.price / comp.size;
            }
            else {
                return null;
            }
        }, this.context.regionComps1Year);
        this.output.region_upper_bed_avg_ppsf_dev = Stats.deviation(this.output.actual_sale_ppsf, this.output.region_upper_bed_avg_ppsf);
        this.output.region_upper_bed_median_ppsf = Stats.getMedianForProperty(comp => {
            if (_.isNumber(comp.price) && _.isNumber(comp.size)) {
                return comp.price / comp.size;
            }
            else {
                return null;
            }
        }, this.context.regionComps1Year);
        this.output.region_upper_bed_median_ppsf_dev = Stats.deviation(this.output.actual_sale_ppsf, this.output.region_upper_bed_avg_ppsf);
        this.output.region_upper_bed_avg_dom = Stats.getAvgForProperty('days_on_market', this.context.regionComps1Year);
        this.output.region_upper_bed_avg_dom_dev = Stats.deviation(this.output.dom, this.output.region_upper_bed_avg_dom);
        const { min, max } = Stats.getActiveMinMaxForProperty('size', this.context.regionComps1Year);
        if (_.isNumber(min)) {
            this.output.region_upper_bed_active_min_sqft = min;
            this.output.region_upper_bed_active_min_sqft_dev = Stats.deviation(this.output.sqft, this.output.region_upper_bed_active_min_sqft);
        }
        else {
            this.output.region_upper_bed_active_min_sqft = this.output.region_upper_bed_active_min_sqft_dev = null;
        }
        if (_.isNumber(max)) {
            this.output.region_upper_bed_active_max_sqft = max;
            this.output.region_upper_bed_active_max_sqft_dev = Stats.deviation(this.output.sqft, this.output.region_upper_bed_active_min_sqft);
        }
        else {
            this.output.region_upper_bed_active_max_sqft = this.output.region_upper_bed_active_max_sqft_dev = null;
        }
        this.output.region_upper_bed_median_monthlies = Stats.getMedianForProperty(comp => {
            if (_.isNumber(comp.maintenance) && _.isNumber(comp.tax)) {
                return comp.maintenance + comp.tax;
            }
            else {
                return null;
            }
        }, this.context.regionComps1Year);
        this.output.region_upper_bed_median_monthlies_dev = Stats.deviation(this.output.monthlies, this.output.region_upper_bed_median_monthlies);
        const count1Yr = Stats.getActPendSoldCount(this.context.regionComps1Year);
        this.output.region_upper_bed_1_yr_active = count1Yr.active;
        this.output.region_upper_bed_1_yr_pending = count1Yr.pending;
        this.output.region_upper_bed_1_yr_sold = count1Yr.sold;
        const count2Yr = Stats.getActPendSoldCount(this.context.regionComps2Year);
        this.output.region_upper_bed_2_yr_active = count2Yr.active;
        this.output.region_upper_bed_2_yr_pending = count2Yr.pending;
        this.output.region_upper_bed_2_yr_sold = count2Yr.sold;
        this.output.region_upper_bed_avg_price = Stats.getAvgForProperty('price', this.context.regionComps1Year);
        this.output.region_upper_bed_avg_price_dev = Stats.deviation(this.output.actual_sale_price, this.output.region_upper_bed_avg_price);
        this.output.region_upper_bed_median_price = Stats.getMedianForProperty('price', this.context.regionComps1Year);
        this.output.region_upper_bed_median_price_dev = Stats.deviation(this.output.actual_sale_price, this.output.region_upper_bed_median_price);
        this.output.region_upper_bed_avg_sqft = Stats.getAvgForProperty('size', this.context.regionComps1Year);
        this.output.region_upper_bed_avg_sqft_dev = Stats.deviation(this.output.sqft, this.output.region_upper_bed_avg_sqft);
        this.output.region_upper_bed_avg_ppsf = Stats.getAvgForProperty(comp => {
            if (_.isNumber(comp.price) && _.isNumber(comp.size)) {
                return comp.price / comp.size;
            }
            else {
                return null;
            }
        }, this.context.regionComps1Year);
        this.output.region_upper_bed_avg_ppsf_dev = Stats.deviation(this.output.actual_sale_ppsf, this.output.region_upper_bed_avg_ppsf);
        this.output.region_upper_bed_median_ppsf = Stats.getMedianForProperty(comp => {
            if (_.isNumber(comp.price) && _.isNumber(comp.size)) {
                return comp.price / comp.size;
            }
            else {
                return null;
            }
        }, this.context.regionComps1Year);
        this.output.region_upper_bed_median_ppsf_dev = Stats.deviation(this.output.actual_sale_ppsf, this.output.region_upper_bed_median_ppsf);
        this.output.region_upper_bed_avg_monthlies = Stats.getAvgForProperty(comp => {
            if (_.isNumber(comp.maintenance) && _.isNumber(comp.tax)) {
                return comp.maintenance + comp.tax;
            }
            else {
                return null;
            }
        }, this.context.regionComps1Year);
        this.output.region_upper_bed_avg_monthlies_dev = Stats.deviation(this.output.monthlies, this.output.region_upper_bed_avg_monthlies);
        this.output.region_upper_bed_avg_dom = Stats.getAvgForProperty('days_on_market', this.context.regionComps1Year);
        this.output.region_upper_bed_avg_price_change = Stats.getAvgForProperty('price_change', this.context.regionComps1Year);
        this.output.region_upper_bed_median_price_change = Stats.getMedianForProperty('price_change', this.context.regionComps1Year);
    }
    setDeals() {
        this.totalAcquisitionCost();
        this.mortgageAmountAcquisitionCost();
        this.monthlyMortgagePayment();
        this.totalSalesCosts();
        this.compitProfit();
        this.calculateDealTerm();
    }
    totalAcquisitionCost() {
        this.output.total_acquisition_cost = Helpers.getTotalAcquisitionCost(this.output.loan_to_value_rate, this.output.bank_mortgage_fee_rate, this.output.actual_purchase_price, this.output.closing_costs_rate, this.output.compit_acquisition_fee_rate, this.output.renovation_rate, this.output.eff_price);
    }
    mortgageAmountAcquisitionCost() {
        this.output.mortgage_amount_acquisition_cost = this.output.loan_to_value_rate * this.output.total_acquisition_cost;
    }
    // TODO: not in db
    monthlyMortgagePayment() {
        if (_.isNumber(this.output.total_acquisition_cost)) {
            this.output.monthly_mortgage_payment =
                (this.output.loan_to_value_rate * this.output.total_acquisition_cost * (this.output.mortgage_rate / 12 * Math.pow((1 + this.output.mortgage_rate / 12), (12 * 30)) / (Math.pow((1 + this.output.mortgage_rate / 12), (12 * 30)) - 1)));
        }
        else {
            this.output.monthly_mortgage_payment = null;
        }
    }
    totalSalesCosts() {
        if (_.isNumber(this.output.actual_sale_price)) {
            this.output.total_sales_costs = (this.output.sale_closing_costs_rate + this.output.sale_seller_broker_fee_rate +
                this.output.sale_buyer_broker_fee_rate + this.output.compit_sell_out_fee_rate) * this.output.actual_sale_price;
        }
        else {
            this.output.total_sales_costs = null;
        }
    }
    compitProfit() {
        this.output.compit_profit = this.output.compit_acquisition_fee_rate * this.output.actual_purchase_price + this.output.actual_sale_price * (this.output.compit_seller_rate * this.output.sale_seller_broker_fee_rate + this.output.compit_sell_out_fee_rate);
    }
    calculateDealTerm() {
        const dealTerm12 = new DealComponent(12, this.output).calculate();
        const dealTerm18 = new DealComponent(18, this.output).calculate();
        this.output.total_holding_costs_12 = dealTerm12.total_holding_costs;
        this.output.total_deal_costs_12 = dealTerm12.total_deal_costs;
        this.output.cap_rate_on_equity_12 = dealTerm12.cap_rate_on_equity;
        this.output.maintenance_fee_price_12 = dealTerm12.maintenance_fee_price;
        this.output.asset_mgmt_fee_price_12 = dealTerm12.asset_mgmt_fee_price;
        this.output.mortgage_price_12 = dealTerm12.mortgage_price;
        this.output.mortgage_amount_holding_cost_12 = dealTerm12.mortgage_amount_holding_cost;
        this.output.total_mortgage_amount_12 = dealTerm12.total_mortgage_amount;
        this.output.total_equity_required_12 = dealTerm12.total_equity_required;
        this.output.return_on_equity_12 = dealTerm12.return_on_equity;
        this.output.return_on_investment_12 = dealTerm12.return_on_investment;
        this.output.roe_12 = dealTerm12.roe;
        this.output.roe_005_0_12 = dealTerm12.roe_005_0;
        this.output.roe_0075_0_12 = dealTerm12.roe_0075_0;
        this.output.roe_01_0_12 = dealTerm12.roe_01_0;
        this.output.roe_0_005_12 = dealTerm12.roe_0_005;
        this.output.roe_005_005_12 = dealTerm12.roe_005_005;
        this.output.roe_0075_005_12 = dealTerm12.roe_0075_005;
        this.output.roe_01_005_12 = dealTerm12.roe_01_005;
        this.output.roe_0_01_12 = dealTerm12.roe_0_01;
        this.output.roe_005_01_12 = dealTerm12.roe_005_01;
        this.output.roe_0075_01_12 = dealTerm12.roe_0075_01;
        this.output.roe_01_01_12 = dealTerm12.roe_01_01;
        this.output.roe_rank_12 = dealTerm12.roe_rank;
        this.output.roi_12 = dealTerm12.roi;
        this.output.total_holding_costs_18 = dealTerm18.total_holding_costs;
        this.output.total_deal_costs_18 = dealTerm18.total_deal_costs;
        this.output.cap_rate_on_equity_18 = dealTerm18.cap_rate_on_equity;
        this.output.maintenance_fee_price_18 = dealTerm18.maintenance_fee_price;
        this.output.asset_mgmt_fee_price_18 = dealTerm18.asset_mgmt_fee_price;
        this.output.mortgage_price_18 = dealTerm18.mortgage_price;
        this.output.mortgage_amount_holding_cost_18 = dealTerm18.mortgage_amount_holding_cost;
        this.output.total_mortgage_amount_18 = dealTerm18.total_mortgage_amount;
        this.output.total_equity_required_18 = dealTerm18.total_equity_required;
        this.output.return_on_equity_18 = dealTerm18.return_on_equity;
        this.output.return_on_investment_18 = dealTerm18.return_on_investment;
        this.output.roe_18 = dealTerm18.roe;
        this.output.roe_005_0_18 = dealTerm12.roe_005_0;
        this.output.roe_0075_0_18 = dealTerm18.roe_0075_0;
        this.output.roe_01_0_18 = dealTerm18.roe_01_0;
        this.output.roe_0_005_18 = dealTerm18.roe_0_005;
        this.output.roe_005_005_18 = dealTerm18.roe_005_005;
        this.output.roe_0075_005_18 = dealTerm18.roe_0075_005;
        this.output.roe_01_005_18 = dealTerm18.roe_01_005;
        this.output.roe_0_01_18 = dealTerm18.roe_0_01;
        this.output.roe_005_01_18 = dealTerm18.roe_005_01;
        this.output.roe_0075_01_18 = dealTerm18.roe_0075_01;
        this.output.roe_01_01_18 = dealTerm18.roe_01_01;
        this.output.roe_rank_18 = dealTerm18.roe_rank;
        this.output.roi_18 = dealTerm18.roi;
        this.output.gross_profit_rate_12 = dealTerm12.gross_profit_rate;
        this.output.gross_profit_12 = dealTerm12.gross_profit;
        this.output.gross_profit_005_0_12 = dealTerm12.gross_profit_005_0;
        this.output.gross_profit_0075_0_12 = dealTerm12.gross_profit_0075_0;
        this.output.gross_profit_01_0_12 = dealTerm12.gross_profit_01_0;
        this.output.gross_profit_0_005_12 = dealTerm12.gross_profit_0_005;
        this.output.gross_profit_005_005_12 = dealTerm12.gross_profit_005_005;
        this.output.gross_profit_0075_005_12 = dealTerm12.gross_profit_0075_005;
        this.output.gross_profit_01_005_12 = dealTerm12.gross_profit_01_005;
        this.output.gross_profit_0_01_12 = dealTerm12.gross_profit_0_01;
        this.output.gross_profit_005_01_12 = dealTerm12.gross_profit_005_01;
        this.output.gross_profit_0075_01_12 = dealTerm12.gross_profit_0075_01;
        this.output.gross_profit_01_01_12 = dealTerm12.gross_profit_01_01;
        this.output.gross_profit_rate_18 = dealTerm18.gross_profit_rate;
        this.output.gross_profit_18 = dealTerm18.gross_profit;
        this.output.gross_profit_005_0_18 = dealTerm18.gross_profit_005_0;
        this.output.gross_profit_0075_0_18 = dealTerm18.gross_profit_0075_0;
        this.output.gross_profit_01_0_18 = dealTerm18.gross_profit_01_0;
        this.output.gross_profit_0_005_18 = dealTerm18.gross_profit_0_005;
        this.output.gross_profit_005_005_18 = dealTerm18.gross_profit_005_005;
        this.output.gross_profit_0075_005_18 = dealTerm18.gross_profit_0075_005;
        this.output.gross_profit_01_005_18 = dealTerm18.gross_profit_01_005;
        this.output.gross_profit_0_01_18 = dealTerm18.gross_profit_0_01;
        this.output.gross_profit_005_01_18 = dealTerm18.gross_profit_005_01;
        this.output.gross_profit_0075_01_18 = dealTerm18.gross_profit_0075_01;
        this.output.gross_profit_01_01_18 = dealTerm18.gross_profit_01_01;
    }
}
module.exports = SubjectTransformer;
