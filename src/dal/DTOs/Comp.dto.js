const camelize = require('camelize');
class CompDTO {
    constructor(comp) {
        this.comp = comp;
        this._map();
        return this;
    }
    _map() {
        this.comp.psf = this.comp.size ? (this.comp.price / this.comp.size) : null;
        this.output = camelize(this.comp);
    }
    static map(comp) {
        return new CompDTO(comp).output;
    }
}
module.exports = CompDTO;
