const camelize = require('camelize');
class SubjectDTO {
    constructor(subject) {
        this.output = {
            region: {},
            neighborhood: {},
            building: {},
            deals: [],
            media: {
                photos: null,
                floorplans: null,
                renovatedFloorplanUrl: null
            }
        };
        this.dealPatterns = ['total_holding_costs', 'total_deal_costs', 'cap_rate_on_equity', 'roe', 'roi', 'gross_profit',
            'maintenance_fee_price', 'asset_mgmt_fee_price', 'mortgage_price', 'mortgage_amount_holding_cost', 'total_mortgage_amount',
            'total_equity_required', 'return_on_equity', 'return_on_investment'];
        this.subject = subject;
        this._map();
        this.mapDeals();
        this.output = camelize(this.output);
        return this;
    }
    _map() {
        for (const property of Object.keys(this.subject)) {
            switch (true) {
                case property === 'photos':
                    this.output.media.photos = JSON.parse(this.subject[property]);
                    break;
                case property === 'floorplans':
                    this.output.media.floorplans = JSON.parse(this.subject[property]);
                    break;
                case property.startsWith('region'): {
                    const prop = property.replace(/^region_/, '');
                    this.output.region[prop] = this.subject[property];
                    break;
                }
                case property.startsWith('neighborhood'): {
                    const prop = property.replace(/^neighborhood_/, '');
                    this.output.neighborhood[prop] = this.subject[property];
                    break;
                }
                case property.startsWith('building'): {
                    const prop = property.replace(/^building_/, '');
                    this.output.building[prop] = this.subject[property];
                    break;
                }
                case this.isDealProperty(property):
                    // Do nothing for now
                    break;
                default:
                    this.output[property] = this.subject[property];
            }
        }
    }
    mapDeals() {
        const s = this.subject;
        const deal12 = {
            dealTerm: 12,
            roe: s.roe_12,
            roeRank: s.roe_rank_12,
            roi: s.roi_12,
            grossProfit: s.gross_profit_12,
            grossProfitRate: s.gross_profit_rate_12,
            totalHoldingCosts: s.total_holding_costs_12,
            totalDealCosts: s.total_deal_costs_12,
            capRateOnEquity: s.cap_rate_on_equity_12,
            maintenanceFeePrice: s.maintenance_fee_price_12,
            assetMgmtFeePrice: s.asset_mgmt_fee_price_12,
            mortgagePrice: s.mortgage_price_12,
            mortgageAmountHoldingCost: s.mortgage_amount_holding_cost_12,
            totalMortgageAmount: s.total_mortgage_amount_12,
            totalEquityRequired: s.total_equity_required_12,
            returnOnEquity: s.return_on_equity_12,
            returnOnInvestment: s.return_on_investment_12,
            roes: []
        };
        if (!this.isPredefined(s.purchase_price_discount, s.sale_price_discount)) {
            deal12.roes.push({ roe: s.roe_12, grossProfit: s.gross_profit_12, purchasePriceDiscount: s.purchase_price_discount, salePriceDiscount: s.sale_price_discount });
        }
        deal12.roes.push({ roe: s.roe_005_0_12, grossProfit: s.gross_profit_005_0_12, purchasePriceDiscount: 0.05, salePriceDiscount: 0 });
        deal12.roes.push({ roe: s.roe_0075_0_12, grossProfit: s.gross_profit_0075_0_12, purchasePriceDiscount: 0.075, salePriceDiscount: 0 });
        deal12.roes.push({ roe: s.roe_01_0_12, grossProfit: s.gross_profit_01_0_12, purchasePriceDiscount: 0.1, salePriceDiscount: 0 });
        deal12.roes.push({ roe: s.roe_0_005_12, grossProfit: s.gross_profit_0_005_12, purchasePriceDiscount: 0, salePriceDiscount: 0.05 });
        deal12.roes.push({ roe: s.roe_005_005_12, grossProfit: s.gross_profit_005_005_12, purchasePriceDiscount: 0.05, salePriceDiscount: 0.05 });
        deal12.roes.push({ roe: s.roe_0075_005_12, grossProfit: s.gross_profit_0075_005_12, purchasePriceDiscount: 0.075, salePriceDiscount: 0.05 });
        deal12.roes.push({ roe: s.roe_01_005_12, grossProfit: s.gross_profit_01_005_12, purchasePriceDiscount: 0.1, salePriceDiscount: 0.05 });
        deal12.roes.push({ roe: s.roe_0_01_12, grossProfit: s.gross_profit_0_01_12, purchasePriceDiscount: 0, salePriceDiscount: 0.1 });
        deal12.roes.push({ roe: s.roe_005_01_12, grossProfit: s.gross_profit_005_01_12, purchasePriceDiscount: 0.05, salePriceDiscount: 0.1 });
        deal12.roes.push({ roe: s.roe_0075_01_12, grossProfit: s.gross_profit_0075_01_12, purchasePriceDiscount: 0.075, salePriceDiscount: 0.1 });
        deal12.roes.push({ roe: s.roe_01_01_12, grossProfit: s.gross_profit_01_01_12, purchasePriceDiscount: 0.1, salePriceDiscount: 0.1 });
        this.output.deals.push(deal12);
        const deal18 = {
            dealTerm: 18,
            roe: s.roe_18,
            roeRank: s.roe_rank_18,
            roi: s.roi_18,
            grossProfit: s.gross_profit_18,
            grossProfitRate: s.gross_profit_rate_18,
            totalHoldingCosts: s.total_holding_costs_18,
            totalDealCosts: s.total_deal_costs_18,
            capRateOnEquity: s.cap_rate_on_equity_18,
            maintenanceFeePrice: s.maintenance_fee_price_18,
            assetMgmtFeePrice: s.asset_mgmt_fee_price_18,
            mortgagePrice: s.mortgage_price_18,
            mortgageAmountHoldingCost: s.mortgage_amount_holding_cost_18,
            totalMortgageAmount: s.total_mortgage_amount_18,
            totalEquityRequired: s.total_equity_required_18,
            returnOnEquity: s.return_on_equity_18,
            returnOnInvestment: s.return_on_investment_18,
            roes: []
        };
        if (!this.isPredefined(s.purchase_price_discount, s.sale_price_discount)) {
            deal18.roes.push({ roe: s.roe_18, grossProfit: s.gross_profit_18, purchasePriceDiscount: s.purchase_price_discount, salePriceDiscount: s.sale_price_discount });
        }
        deal18.roes.push({ roe: s.roe_005_0_18, grossProfit: s.gross_profit_005_0_18, purchasePriceDiscount: 0.05, salePriceDiscount: 0 });
        deal18.roes.push({ roe: s.roe_0075_0_18, grossProfit: s.gross_profit_0075_0_18, purchasePriceDiscount: 0.075, salePriceDiscount: 0 });
        deal18.roes.push({ roe: s.roe_01_0_18, grossProfit: s.gross_profit_01_0_18, purchasePriceDiscount: 0.1, salePriceDiscount: 0 });
        deal18.roes.push({ roe: s.roe_0_005_18, grossProfit: s.gross_profit_0_005_18, purchasePriceDiscount: 0, salePriceDiscount: 0.05 });
        deal18.roes.push({ roe: s.roe_005_005_18, grossProfit: s.gross_profit_005_005_18, purchasePriceDiscount: 0.05, salePriceDiscount: 0.05 });
        deal18.roes.push({ roe: s.roe_0075_005_18, grossProfit: s.gross_profit_0075_005_18, purchasePriceDiscount: 0.075, salePriceDiscount: 0.05 });
        deal18.roes.push({ roe: s.roe_01_005_18, grossProfit: s.gross_profit_01_005_18, purchasePriceDiscount: 0.1, salePriceDiscount: 0.05 });
        deal18.roes.push({ roe: s.roe_0_01_18, grossProfit: s.gross_profit_0_01_18, purchasePriceDiscount: 0, salePriceDiscount: 0.1 });
        deal18.roes.push({ roe: s.roe_005_01_18, grossProfit: s.gross_profit_005_01_18, purchasePriceDiscount: 0.05, salePriceDiscount: 0.1 });
        deal18.roes.push({ roe: s.roe_0075_01_18, grossProfit: s.gross_profit_0075_01_18, purchasePriceDiscount: 0.075, salePriceDiscount: 0.1 });
        deal18.roes.push({ roe: s.roe_01_01_18, grossProfit: s.gross_profit_01_01_18, purchasePriceDiscount: 0.1, salePriceDiscount: 0.1 });
        this.output.deals.push(deal18);
    }
    isDealProperty(property) {
        let isDealProperty = false;
        for (const pattern of this.dealPatterns) {
            if (property.startsWith(pattern)) {
                isDealProperty = true;
                break;
            }
        }
        return isDealProperty;
    }
    isPredefined(purchasePriceDiscount, salePriceDiscount) {
        const pairs = [[0.05, 0], [0.075, 0], [0.1, 0], [0, 0.05], [0.05, 0.05], [0.075, 0.05], [0.1, 0.05], [0, 0.1], [0.05, 0.1], [0.075, 0.1], [0.1, 0.1]];
        return pairs.find(pair => {
            return pair[0] === purchasePriceDiscount && pair[1] === salePriceDiscount;
        });
    }
    static map(subject) {
        return new SubjectDTO(subject).output;
    }
}
module.exports = SubjectDTO;
