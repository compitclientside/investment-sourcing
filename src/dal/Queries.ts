const config = require('config')

export default class Queries {

    static getCalculatedSubjects (page?: number, size?: number): String {
      let query = `SELECT * FROM investments_sourcing`
      if (size) query += ` LIMIT ${size}`
      if (page) query += ` OFFSET ${page * size}`
      return query
    };
  
    static getCalculatedSubjectByEntityId (entityId: String, upperBeds: number, upperBaths: number): String {
      return `SELECT * FROM investments_sourcing WHERE entity_id = '${entityId}' AND new_beds = '${upperBeds}' AND new_baths = '${upperBaths}';`
    }

    // TODO: do we need here the join with region?
    static getRawSubjects (entityId?: String): String { // entityId = 'USA:NY:New York:Manhattan:Central Park West:1:32F'
      const db = config.propertyDb;
      let query = `
        SELECT
          rg.region,
          nb.BoroughId AS borough_id,
          nb.id AS neighborhood_id,
          nb.NeighborhoodsGroup AS neighborhoods_group,
          addrr.address,
          addrr.longitude,
          addrr.latitude,
          build.id AS building_id,
          build.floors,
          build.ownership,
          build.developmentStatus AS development_status,
          apts.id AS apartment_id,
          apts.entityId AS entity_id,
          apts.unit,
          apts.floor,
          apts.line,
          acts.status,
          acts.price,
          acts.actionType AS action_type,
          acts.effectiveDate AS effective_date,
          acts.daysOnMarket AS days_on_market,
          acts.CycleId AS cycle_id,
          av.indice AS indexed_price,
          aa1.approx AS sale_approx,
          aa1.soughtAfterProperty as sale_sought_after_property,
          aa2.approx AS rent_approx,
          aa2.soughtAfterProperty as rent_sought_after_property,
          NVL(ld.beds, apts.beds) AS beds,
          NVL(ld.baths, apts.baths) AS baths,
          ld.rooms,
          NVL(ld.size, apts.size) AS size,
          ld.maintenance,
          ld.tax,
          ld.url,
          ld.description,
          conv.convertible,
          cycs.priceChangePct AS discount_pct,
          cycs.entityId AS listing_entity_id
        FROM ${db}.Cycles cycs
        JOIN ${db}.Actions acts on cycs.id = acts.CycleId AND cycs.isLast = 1 AND acts.isClosing = 1
        LEFT JOIN ${db}.ListingData ld ON cycs.id = ld.CycleId AND ld.isLastInCycle = 1
        JOIN ${db}.Apartments apts ON cycs.ApartmentId = apts.id
        JOIN ${db}.Buildings build ON apts.BuildingId = build.id
        JOIN ${db}.Addresses addrr ON build.primaryaddressid = addrr.id
        JOIN ${db}.Neighborhoods nb ON build.Neighborhoodid = nb.id
        JOIN public.region rg ON nb.id = rg.Neighborhoodid
        LEFT JOIN ${db}.apartmentsindices av ON cycs.entityid = av.listingentityid
        LEFT JOIN public.apartmentapproxes aa1 on apts.entityid = aa1.apartmententityid AND aa1.listingtype = 'sale'
        LEFT JOIN public.apartmentapproxes aa2 on apts.entityid = aa2.apartmententityid AND aa2.listingtype = 'rent'
        LEFT JOIN stgProperty.Convertibles conv ON conv.unit = apts.unit AND conv.address = addrr.address AND conv.BoroughId = nb.BoroughId
        WHERE cycs.confirmationLevel IS NULL
        AND acts.actionType = 'SaleUpdate'
        AND acts.status = 'active'`;
  
        if (entityId) 
          query += ` AND apts.entityId = '${entityId}'`;
  
        query += ` AND apts.unit NOT LIKE 'house' 
        AND (apts.floor > 1 OR apts.floor IS NULL) 
        AND (apts.floorType = 'midfloor' OR apts.floorType IS NULL)
        AND build.ownership = 'condo'`
      return query
    }
    // TODO: filter off market?
    static getComps (region: String, beds?: number, baths?: number, days?: number): String {
      const db = config.propertyDb;
      let query = `
        SELECT
          nb.id AS neighborhood_id,
          nb.name AS neighborhood_name,
          nb.NeighborhoodsGroup AS neighborhoods_group,
          nb.Boroughid AS borough_id,
          builds.id AS building_id,
          builds.ownership,
          builds.developmentStatus AS development_status,
          addrrs.address,
          addrrs.longitude,
          addrrs.latitude,
          apts.id AS apartment_id,
          apts.entityId AS entity_id,
          apts.unit,
          acts.price,
          acts.status,
          acts.actionType AS action_type,
          acts.effectiveDate AS listing_date,
          acts.daysOnMarket AS days_on_market,
          NVL(lds.beds, apts.beds) AS beds,
          NVL(lds.baths, apts.baths) AS baths,
          NVL(lds.size, apts.size) AS size,
          lds.maintenance,
          lds.tax,
          (SELECT daysOnMarket FROM ${db}.Actions WHERE ApartmentId = apts.id AND CycleId = cycs.id AND isPending = 1) AS pending_dom,
          cycs.priceChangePct AS price_change_pct,
          cycs.priceChange AS price_change
        FROM ${db}.Actions acts
        JOIN ${db}.Cycles cycs ON acts.CycleId = cycs.id
        LEFT JOIN ${db}.Listingdata lds ON cycs.id = lds.CycleId AND lds.isLastInCycle = 1
        JOIN ${db}.Apartments apts ON acts.ApartmentId = apts.id
        JOIN ${db}.Buildings builds ON apts.BuildingId = builds.id
        JOIN ${db}.Addresses addrrs ON builds.primaryaddressid = addrrs.id
        JOIN ${db}.Neighborhoods nb ON builds.Neighborhoodid = nb.id
        WHERE acts.isClosing = 1
        AND acts.actionType = 'SaleUpdate'
        AND acts.actionDate > CURRENT_TIMESTAMP - INTERVAL '720 days'`;

        if (beds && baths) {
          query += ` AND NVL(lds.beds, apts.beds) = ${beds} AND NVL(lds.baths, apts.baths) >= ${baths}`
        }
        
        query += ` AND builds.ownership = 'condo'
        AND nb.id IN (
          SELECT Neighborhoodid FROM public.region WHERE region = '${region}'
        )`
      return query
    }
    
    // TODO: check whether we can filter by approx value
    static getIndexes (region: string): string {
      const db = config.propertyDb;
      const query = `
        SELECT
          nb.NeighborhoodsGroup AS neighborhoods_group,
          builds.id AS building_id,
          apts.id AS apartment_id,
          apts.beds,
          apts.baths,
          apts.size,
          aa.approx,
          aa.listingType AS listing_type
        FROM ${db}.Apartments apts
        JOIN ${db}.Buildings builds ON apts.BuildingId = builds.id
        JOIN ${db}.Neighborhoods nb ON builds.Neighborhoodid = nb.id
        JOIN public.apartmentapproxes aa ON apts.entityId = aa.apartmententityid
        AND aa.listingType = 'rent' AND approx IS NOT NULL
        AND nb.id IN (
          SELECT Neighborhoodid FROM public.region WHERE region = '${region}'
        )
      `
    return query
    }

    static getCalculatedSubjectLastClosingActions (apartmentId: Number): string {
      const db = config.propertyDb;
      const query = `
        SELECT 
          actionDate AS action_date,
          price,
          status,
          daysOnMarket AS days_on_market,
          priceChangePct AS price_change_pct
        FROM ${db}.Actions acts
        JOIN ${db}.Apartments apts ON acts.ApartmentId = apts.id
        WHERE Apartmentid = ${apartmentId}
        AND acts.status IN ('sold', 'rented')
        AND acts.isClosing = 1
        AND acts.silent IS NULL
        ORDER BY actionDate DESC;`
      return query
    }
  
    static getCalculatedSubjectLastEqualSubjectSameLine (beds: number, baths: number, apartmentId: number, buildingId: number, line: string) { 
      return Queries.geCalculatedSubjecttLastEqualSubject(beds, baths, apartmentId, buildingId, line, true)
    }
  
    static getCalculatedSubjectLastEqualSubjectDifferentLine(beds: number, baths: number, apartmentId: number, buildingId: number, line: string) { 
      return Queries.geCalculatedSubjecttLastEqualSubject(beds, baths, apartmentId, buildingId, line, false)
    }
  
    static geCalculatedSubjecttLastEqualSubject (beds: number, baths: number, apartmentId: number, buildingId: number, line: string, isSameLine: boolean) {
      const db = config.propertyDb;
      let query = `
        SELECT
          apts.id AS apartment_id,
          apts.unit,
          apts.line,
          apts.beds,
          apts.baths,
          acts.price,
          acts.status,
          acts.actionDate AS action_date,
          acts.actionType AS action_type  
        FROM ${db}.Actions acts 
        JOIN ${db}.Apartments apts ON acts.ApartmentId = apts.id 
        JOIN ${db}.Buildings builds ON apts.BuildingId = builds.id
        WHERE builds.id = ${buildingId}`;
  
      if (isSameLine) {
        query += ` AND apts.line = '${line}' AND apts.id <> ${apartmentId}`
      } else {
        query += ` AND apts.line <> '${line}'`
      }
  
      query += `
        AND apts.beds = ${beds}
        AND apts.baths = ${baths}
        AND acts.status IN ('active', 'pending', 'sold')
        AND acts.actionType = 'SaleUpdate'
        AND acts.isClosing = 1
        ORDER BY acts.actionDate DESC LIMIT 1;`
      return query
    }

    static getMediaByCycleId (id: number) {
      const db = config.propertyDb;
      return `SELECT m.type, m.url FROM ${db}.CycleMedia cm JOIN ${db}.Media m ON cm.MediaId = m.id WHERE cm.CycleId = ${id}`
    }

    static clear () {
      return `TRUNCATE investments_sourcing;`
    }
  }
