const pg = require('pg');
const logger = require('../../common/logger');
class RedshiftClient {
    constructor(config) {
        this.client = new pg.Client(config);
        RedshiftClient.instance = this;
    }
    async connect() {
        logger.info('Connecting to Redshift', { env: process.env.NODE_ENV });
        await this.client.connect();
    }
    async query(query) {
        try {
            return await this.client.query(query);
        }
        catch (err) {
            logger.error(err);
        }
    }
    async close() {
        logger.info('Closing Redshift', { env: process.env.NODE_ENV });
        await this.client.end();
    }
    static getInstance() {
        return RedshiftClient.instance;
    }
}
module.exports = RedshiftClient;
