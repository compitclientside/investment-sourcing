const AWS = require('aws-sdk');
class FirehoseClient {
    constructor({ region, deliveryStream, bulkSize = 500 }) {
        this.deliveryStream = deliveryStream;
        this.firehose = new AWS.Firehose({ region });
        this.bulkSize = bulkSize;
    }
    async pushMany(records) {
        let index = 0;
        while (index < records.length) {
            const bulk = records.slice(index, index + this.bulkSize);
            const bulkData = bulk.map(record => {
                const newRecord = {};
                for (const key of Object.keys(record)) {
                    const keyLower = key.toLowerCase();
                    newRecord[keyLower] = record[key];
                }
                return { Data: JSON.stringify(newRecord) };
            });
            await this.firehose.putRecordBatch({
                Records: bulkData,
                DeliveryStreamName: this.deliveryStream
            }).promise();
            index += this.bulkSize;
        }
    }
}
module.exports = FirehoseClient;
