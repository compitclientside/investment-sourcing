
/**
 * @file manages the batch process of fetching all raw subjects,
 * apply transformation and load all subjects to Redshift to the 'investments' table
 */

const path = require('path');
process.env.NODE_CONFIG_DIR = path.join(__dirname, '../../config');
const config = require('config');
const program = require('commander');
const redshiftClient = require('../dal/clients/Redshift.client');
const Investments = require('../Investments');
const Helpers = require('../common/Helpers');
const logger = require('../common/logger');
program
    .option('--arg', 'Description')
    .parse(process.argv);
!(async function load() {
    // const start = new Date();
    let buffer = [];
    try {
        logger.info('Strting load.js', { env: process.env.NODE_ENV });
        const dbConfig = {
            host: config.aws.redshift.host,
            port: config.aws.redshift.port,
            user: config.aws.redshift.user,
            password: config.aws.redshift.password,
            database: config.aws.redshift.database
        };
        const db = new redshiftClient(dbConfig);
        await db.connect();
        const investments = new Investments(db);
        await investments.initialize();
        await investments.clear();
        const rawSubjects = await investments.getRawSubjects();
        logger.info('Got raw subjects', { count: rawSubjects.length });
        const filteredRawSubjects = investments.filter(rawSubjects);
        logger.info('Filtered raw subjects', { count: filteredRawSubjects.length });
        for (let subject of filteredRawSubjects) {
            try {
                subject = Helpers.castToNumber(subject)[0];
                const upperBedsBathsPairs = investments.getUpperBedsBaths(subject.beds, subject.baths, subject.size);
                subject.media = await investments.getMediaByCycleId(subject.cycle_id);
                for (const pair of upperBedsBathsPairs) {
                    const context = await investments.getContextForSubject(subject, pair.beds, pair.baths);
                    const index = await investments.getIndexForSubject(subject, pair.beds, pair.baths);
                    const subjectTransformer = investments.getSubjectTransformer(subject, context, index, pair.beds, pair.baths, config.defaultStaticFields);
                    const output = await subjectTransformer.transform();
                    logger.info('Done processing single subject, adding to buffer', { entityId: subject.entity_id });
                    buffer.push(output);
                }
                if (buffer.length >= Investments.CHUNK_SIZE_TO_LOAD) {
                    logger.info('Loading buffer to Redshift');
                    await investments.load(buffer);
                    buffer = [];
                }
            }
            catch (err) {
                logger.error(err.message);
            }
        }
        // The buffer is not empty it has less than CHUNK_SIZE_TO_LOAD
        if (buffer.length) {
            logger.info('Loading final buffer to Redshift', { count: buffer.length });
            await investments.load(buffer);
        }
        // const end = new Date();
        // const time = (end - start) / 1000;
        //logger.info('Done processing all subjects', { count: subjects.length, time, fails: subjects.length - buffer.length, success: buffer.length })
        process.exitCode = 0;
    }
    catch (err) {
        logger.error(err.message);
        process.exitCode = 1;
    }
    setTimeout(process.exit, 5000);
})();
