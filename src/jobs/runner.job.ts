import { join } from 'path'

process.env.NODE_CONFIG_DIR = join(__dirname, '../../config')
const config = require('config')
/**
 * @file Simple script for testing different scenarios
 */

import * as _ from 'lodash'
import RedshiftClient from '../dal/clients/Redshift.client'
import Investments from  '../Investments'
import logger from '../common/logger'

!(async function runner() {
  
  try {
    logger.info('Strting runner.js', { env: process.env.NODE_ENV })
    
    const dbConfig = {
      host: config.aws.redshift.host,
      port: config.aws.redshift.port,
      user: config.aws.redshift.user,
      password: config.aws.redshift.password,
      database: config.aws.redshift.database
    }
    
    const db = new RedshiftClient(dbConfig)
    await db.connect()
    const investments = new Investments(db)
    await investments.initialize()

    const results = await investments.getCalculatedSubjectByEntityId('USA:NY:New York:Manhattan:Central Park West:1:32F', 2, 2.5)
    console.log(results)
    
    // Edit
    
    // const params = { 
    //   purchase_price_discount: 0.1,
    //   sale_price_discount: 0.1,
    //   deal_term: 18,
    //   renovation_rate: 0.11,
    //   closing_costs_rate: 0.03,
    //   bank_mortgage_fee_rate: 0.005,
    //   compit_acquisition_fee_rate: 0.01,
    //   maintenance_fee_rate: 0.02,
    //   mortgage_rate: 0.045,
    //   asset_mgmt_fee_rate: 0.015,
    //   loan_to_value_rate: 0.70,
    //   sale_closing_costs_rate: 0.02,
    //   sale_seller_broker_fee_rate: 0.025,
    //   compit_seller_rate: 0.25,
    //   sale_buyer_broker_fee_rate: 0.025,
    //   compit_sell_out_fee_rate: 0.01,
    //   new_beds: 3,
    //   new_baths: 3,
    //   //actual_purchase_price: 600000
    // }
    // const finalParams = Object.assign(config.defaultStaticFields, params)
    // const results = await investments.generate('USA:NY:New York:Manhattan:William Street:15:9D', finalParams)

    // console.log(results.totalAcquisitionCost)
    // console.log('--------------------------')
    // console.log(results.actualPurchasePrice+results.renovationPrice+results.closingCostsPrice+results.bankMortgageFeePrice+results.compitAcquisitionFeePrice)
    // console.log(results.actualPurchasePrice)
    // console.log(results.renovationPrice)
    // console.log(results.closingCostsPrice)
    // console.log(results.bankMortgageFeePrice)
    // console.log(results.compitAcquisitionFeePrice)
    //console.log(results.deals[0].roes[0])
    
    process.exitCode = 0
  } catch (err) {
    logger.error(err.message)
    process.exitCode = 1
  }

  setTimeout(process.exit, 5000)
})()
