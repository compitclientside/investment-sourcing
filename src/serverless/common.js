class Common {
    constructor() {
        this.dbConfig = {
            user: process.env.AWS_REDSHIFT_USER,
            password: process.env.AWS_REDSHIFT_PASS,
            host: process.env.AWS_REDSHIFT_HOST,
            port: process.env.AWS_REDSHIFT_PORT,
            database: process.env.AWS_REDSHIFT_DB
        };
    }
}
module.exports = new Common();
