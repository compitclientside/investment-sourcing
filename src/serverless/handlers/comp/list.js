const path = require('path');
const _ = require('lodash');

process.env.NODE_CONFIG_DIR = path.join(__dirname, '../../../../config');
const common = require('../../common');
const Investments = require('../../../Investments');
const redshiftClient = require('../../../dal/clients/Redshift.client');
const db = new redshiftClient(common.dbConfig);

db.connect();
module.exports.list = async (event) => {
  const { region } = event;
  const bId = parseInt(event.bId);
  const beds = parseInt(event.beds);
  const baths = parseInt(event.baths);
  const nId = parseInt(event.nId);
  // if (_.isNaN(bId)) throw new Error('Invalid bId (buildingId) param');
  // if (_.isNaN(nId)) throw new Error('Invalid nId (Neighborhoods Group Id) param');
  if (_.isNaN(beds)) throw new Error('Invalid beds param');
  if (_.isNaN(baths)) throw new Error('Invalid baths param');
  if (_.isEmpty(region)) throw new Error('Missing region (Region name)');

  const investments = new Investments(db);
  await investments.initialize()
  const res = await investments.getCompsBeautify(region, beds, baths, nId, bId)
  return res;
};
