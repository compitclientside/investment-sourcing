const path = require('path');
process.env.NODE_CONFIG_DIR = path.join(__dirname, '../../../../config');
const config = require('config');
const camelize = require('camelize');
const snake = require('to-snake-case');
const common = require('../../common');
const Investments = require('../../../Investments');
const redshiftClient = require('../../../dal/clients/Redshift.client');
const db = new redshiftClient(common.dbConfig);
db.connect();
module.exports.get = async (event) => {
  // Extract query params
  const defaultStaticFieldsCamelized = camelize(config.defaultStaticFields);
  const params = { new_beds: null, new_baths: null, target_sale_price: null, renovation_price: null, actual_purchase_price: null };
  params.new_beds = parseFloat(event.newBeds);
  params.new_baths = parseFloat(event.newBaths);
  if (isNaN(params.new_beds) || isNaN(params.new_baths)) {
    throw new Error('Invalid or missing newBeds newBaths args');
  }
  // Extract editable fields which exist in the default.json file.
  for (const prop of Object.keys(defaultStaticFieldsCamelized)) {
    if (event[prop])
      params[snake(prop)] = parseFloat(event[prop]);
  }
  // Extract other editable fields.
  if (event.targetSalePrice)
    params.target_sale_price = parseFloat(event.targetSalePrice);
  if (event.renovationPrice)
    params.renovation_price = parseFloat(event.renovationPrice);
  if (event.actualPurchasePrice)
    params.actual_purchase_price = parseFloat(event.actualPurchasePrice);
  const finalParams = Object.assign({}, config.defaultStaticFields, params);
  const investments = new Investments(db);
  await investments.initialize();
  
  const res = await investments.generate(decodeURI(event.entityId), finalParams);
  return res;
};
