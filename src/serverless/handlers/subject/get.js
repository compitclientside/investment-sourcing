const path = require('path');
process.env.NODE_CONFIG_DIR = path.join(__dirname, '../../../../config');
const common = require('../../common');
const Investments = require('../../../Investments');
const redshiftClient = require('../../../dal/clients/Redshift.client');
const db = new redshiftClient(common.dbConfig);
db.connect();
module.exports.get = async (event) => {
    const upperBeds = parseFloat(event.beds);
    const upperBaths = parseFloat(event.baths);
    // Basic valildation.
    if (isNaN(upperBeds) || isNaN(upperBaths)) {
        throw new Error('Invalid beds baths args');
    }
    const investments = new Investments(db);
    await investments.initialize();
    const res = await investments.getCalculatedSubjectByEntityId(decodeURI(event.entityId), upperBeds, upperBaths);
    return res;
};
