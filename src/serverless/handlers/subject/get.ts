import { join } from 'path'
process.env.NODE_CONFIG_DIR = join(__dirname, '../../../../config')

import common from '../../common'
import Investments from  '../../../Investments'
import RedshiftClient from  '../../../dal/clients/Redshift.client'
import logger from '../../../common/logger'

const db = new RedshiftClient(common.dbConfig)
db.connect()

module.exports.get = (event, context, callback) => {
  context.callbackWaitsForEmptyEventLoop = false
  
  const upperBeds = parseFloat(event.beds)
  const upperBaths = parseFloat(event.baths)
  
  // Basic valildation.
  if (isNaN(upperBeds) || isNaN(upperBaths)) {
    common.handleError(new Error('Invalid beds baths args'), callback)
  }
  
  const investments = new Investments(db)

  // Print Debug.
  console.log(`Request event object: ${JSON.stringify(event)}`)
  console.log(`Type of db: ${typeof db}`)
  console.log(`Is db null?: ${db === null}`)
  if (db && typeof db === 'object') {
    console.log(`Constructor name for db object: ${db.constructor.name}`)
    if (db.client) {
      console.log(`Client host: ${db.client.host}`)
      console.log(`Client database: ${db.client.database}`)
      console.log(`Client user: ${db.client.user}`)
      console.log(`Client connected: ${db.client._connected}`)
    } else {
      console.log(`db.client has falsy value: ${db.client}`)
    }
  }

  investments.initialize().then(() => {   
    investments.getCalculatedSubjectByEntityId(decodeURI(event.entityId), upperBeds, upperBaths).then(res => {
      
      // Print debug.
      console.log(`Client response: ${JSON.stringify(res)}`)

      callback(null, res)
    })
    .catch(err => common.handleError(err, callback))
  })
  .catch(err => common.handleError(err, callback))
}
