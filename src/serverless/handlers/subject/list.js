const path = require('path');
process.env.NODE_CONFIG_DIR = path.join(__dirname, '../../../../config');
const _ = require('lodash');
const common = require('../../common');
const Investments = require('../../../Investments');
const redshiftClient = require('../../../dal/clients/Redshift.client');
const { paging: { size: defaultSize } } = require('config');
const db = new redshiftClient(common.dbConfig);
db.connect();
module.exports.list = async (event) => {
  let page = parseInt(event.page);
  let size = parseInt(event.size);
  if (_.isNaN(page)) page = 0;
  if (_.isNaN(size)) size = defaultSize;
  const investments = new Investments(db);
  await investments.initialize()
  const res = await investments.getCalculatedSubjects(page, size);
  return res;
};
