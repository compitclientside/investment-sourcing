const _ = require('lodash');
class BedBathService {
    
    getUpperBedsBaths(beds, baths) {
      if (isNaN(beds) || isNaN(baths)) {
          throw new Error(`BedsBathsService::getNewBedsBaths Invalid beds/baths/sqft values, beds: ${beds}, baths: ${baths}`);
      }
      const bedsBathsList = [];
      
      if(baths - beds == 1){
        bedsBathsList.push({beds: beds+1, baths})
      }
      else if(baths - beds == .5){
        bedsBathsList.push({beds: beds+1, baths}, {beds: beds+1, baths: baths+.5})
      }
      else if(beds - baths == .5 | beds - baths == 1){
        bedsBathsList.push({beds, baths: baths + .5}, {beds, baths: baths + 1})
      }
      
      return bedsBathsList;
  }
}
module.exports = BedBathService;
