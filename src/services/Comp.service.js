const _ = require('lodash');
const Queries = require('../dal/Queries');
const CompDto = require('../dal/DTOs/Comp.dto');
const Helpers = require('../common/Helpers');
const logger = require('../common/logger');
class CompService {
    constructor(db, parent) {
        this.db = db;
        this.parent = parent;
        this.cache = {};
    }
    /**
     * Fetch region comps from db.
     *
     * @param {string} regionName
     * @param {number} [beds]
     * @param {number} [baths]
     * @returns
     * @memberof CompService
     */
    async getComps(regionName, beds, baths) {
        const query = Queries.getComps(regionName, beds, baths);
        const results = await this.db.query(query);
        // TODO: add validation and error handling
        if (results && _.isArray(results.rows)) {
            return results.rows;
        }
    }
    /**
     * Fetch/get region comps from db/cache
     *
     * @param {string} regionName
     * @returns
     * @memberof CompService
     */
    async getCachedComps(regionName) {
        if (regionName in this.cache) {
            logger.info('Getting all region comps from cache', { regionName });
            const rows = this.cache[regionName];
            return rows;
        }
        else {
            logger.info('Getting all region comps from db and setting in cache', { regionName });
            const comps = await this.getComps(regionName);
            this.cache[regionName] = comps;
            return comps;
        }
    }
    /**
     * Fetch/get region comps from db/cache
     * returns all comps splitted to 3 groups - region,neighborhood & builidng
     *
     * @param {string} regionName
     * @param {string} neighborhoodsGroup
     * @param {any} blId
     * @param {any} beds
     * @param {any} baths
     * @returns
     * @memberof CompService
     */
    async getCachedCompsForSubject(regionName, neighborhoodsGroup, blId) {
        let cachedComps = await this.getCachedComps(regionName);
        cachedComps = Helpers.castToNumber(cachedComps);
        return CompService.splitCompsToRgNbBl(cachedComps, neighborhoodsGroup, blId);
    }
    /**
     * Fetch comps, split them to 3 gorups - region,neighborhood and building
     * and convert them to friendly object to be consumed by a client
     *
     * @param {string} regionName
     * @param {number} beds
     * @param {number} baths
     * @param {string} neighborhoodsGroup
     * @param {number} blId
     * @returns
     * @memberof CompService
     */
    async getCompsBeautify(regionName, beds, baths, neighborhoodsGroup, blId) {
        const comps = await this.getComps(regionName, beds, baths);
        let { region, neighborhood, building } = CompService.splitCompsToRgNbBl(comps, neighborhoodsGroup, blId);
        return CompService.beautify({ region, neighborhood, building });
    }
    /**
     * Split comps to 3 groups - region, neighborhood * building
     *
     * @static
     * @param {any[]} comps
     * @param {number} ngId
     * @param {number} blId
     * @returns
     * @memberof CompService
     */
    static splitCompsToRgNbBl(comps, neighborhoodsGroup, blId) {
        const region = comps;
        const neighborhood = comps.filter(comp => comp.neighborhoods_group === neighborhoodsGroup);
        const building = comps.filter(comp => comp.building_id === blId);
        return { region, neighborhood, building };
    }
    static beautify({ region, neighborhood, building }) {
        region = region.map(comp => CompDto.map(comp));
        neighborhood = neighborhood.map(comp => CompDto.map(comp));
        building = building.map(comp => CompDto.map(comp));
        return { region, neighborhood, building };
    }
}
module.exports = CompService;
