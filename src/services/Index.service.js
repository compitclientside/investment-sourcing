const _ = require('lodash');
const Queries = require('../dal/Queries');
const Helpers = require('../common/Helpers');
const logger = require('../common/logger');
class IndexService {
    constructor(db, parent) {
        this.db = db;
        this.parent = parent;
        this.cache = {};
    }
    async getIndexes(region) {
        const query = Queries.getIndexes(region);
        const results = await this.db.query(query);
        // TODO: add validation and error handling
        if (results && _.isArray(results.rows)) {
            return results.rows;
        }
    }
    async getCachedIndexes(region) {
        if (region in this.cache) {
            logger.info('Getting all region indexes from cache', { region });
            const rows = this.cache[region];
            return rows;
        }
        else {
            logger.info('Getting all region indexes from db and setting in cache', { region });
            const indexes = await this.getIndexes(region);
            this.cache[region] = indexes;
            return indexes;
        }
    }
    async getCachedIndexesForSubject(region, neighborhoodsGroup, bId, upperBeds, upperBaths) {
        let cachedIndexes = await this.getCachedIndexes(region);
        cachedIndexes = Helpers.castToNumber(cachedIndexes);
        const filteredIndexes = IndexService.filterByUpperBedsBathsIndexes(cachedIndexes, upperBeds, upperBaths);
        return IndexService.splitCompsToRgNbBl(filteredIndexes, neighborhoodsGroup, bId);
    }
    static filterByUpperBedsBathsIndexes(indexes, upperBeds, upperBaths) {
        return indexes.filter(index => {
            return !isNaN(index.beds) && !isNaN(index.baths) &&
                index.beds === upperBeds &&
                (index.baths === upperBaths || index.baths === upperBaths - 0.5);
        });
    }
    static splitCompsToRgNbBl(indexes, neighborhoodsGroup, bId) {
        const region = indexes;
        const neighborhood = indexes.filter(index => index.neighborhoods_group === neighborhoodsGroup);
        const building = neighborhood.filter(index => index.building_id === bId);
        return { region, neighborhood, building };
    }
}
module.exports = IndexService;
