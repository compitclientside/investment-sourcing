const Queries = require('../dal/Queries');
class MediaService {
    constructor(db, parent) {
        this.db = db;
        this.parent = parent;
    }
    async getByCycleId(cycleId) {
        const query = Queries.getMediaByCycleId(cycleId);
        const results = await this.db.query(query);
        return results.rows;
    }
}
module.exports = MediaService;
