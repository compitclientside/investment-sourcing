import * as _ from 'lodash'
import Queries from '../dal/Queries'
import  SubjectDTO from '../dal/DTOs/Subject.dto'
import Helpers from '../common/Helpers'
import CompService from '../services/Comp.service'
import IndexService from '../services/Index.service'
import Context from '../components/Context.component'
import Index from '../components/Index.component'
import logger from '../common/logger'

export default class SubjectService {
  
  db
  parent

  constructor(db, parent) {
    this.db = db
    this.parent = parent
  }
    
  /**
   * Fetch raw subjects for transformation
   * 
   * @returns {Promise<Object[]>} 
   * @memberof SubjectService
   */
  async getRawSubjects (): Promise<Object[]> {    
    const query: String = Queries.getRawSubjects()
    const results = await this.db.query(query)
    return results.rows
  }
  
  /**
   * Fetch single raw subjects for transformation
   * 
   * @param {string} entityId 
   * @returns 
   * @memberof SubjectService
   */
  async getRawSubjectByEntityId (entityId: string) {
    const query: String = Queries.getRawSubjects(entityId)
    const results = await this.db.query(query)
    if (results && _.isArray(results.rows) && !_.isEmpty(results.rows)) {
      return results.rows[0]
    } else {
      throw new Error(`entityId: ${entityId} wasn't found`)
    }
  }

  /**
   * Fetch post transformed subjects list from the 'investments' table and returns
   * the list in a friendly structure which is easy to consume by a client
   * This method doesn't return each subject comps
   * 
   * @param {number} page 
   * @param {number} size 
   * @returns {Promise<Object[]>} 
   * @memberof SubjectService
   */
  async getCalculatedSubjects(page: number, size: number): Promise<Object[]> {    
    const query: String = Queries.getCalculatedSubjects(page, size)
    const results = await this.db.query(query)
    return results.rows.map(calculatedSubject => {
      return SubjectDTO.map(calculatedSubject)
    })
  }

  /**
   * Fetch single post transformed subject from the 'investments' table
   * returns it in a friendly structure which is easy to consume by a client
   * This method returns all subject comps as well
   * 
   * @param {any} entityId 
   * @returns {Promise<any>} 
   * @memberof SubjectService
   */
  async getCalculatedSubjectByEntityId(entityId: string, upperBeds: number, upperBaths, number): Promise<any> {
    const query: String = Queries.getCalculatedSubjectByEntityId(entityId, upperBeds, upperBaths)
    const results = await this.db.query(query)

    if (results && _.isArray(results.rows) && !_.isEmpty(results.rows)) {
      const subject = results.rows[0] 
      subject.comps = await this.parent.container.compService.getCompsBeautify(
        subject.region, 
        subject.new_beds, 
        subject.new_baths, 
        subject.neighborhoods_group,
        subject.building_id)
      return SubjectDTO.map(subject)
    } else {
      throw new Error(`entityId: ${entityId} with beds: ${upperBeds} baths: ${upperBaths} wasn't found`)
    }
  }

  /**
   * Get the subject's last sold & rent action object
   * 
   * @param {any} apartmentId 
   * @returns 
   * @memberof SubjectService
   */
  async getCalculatedSubjectLastClosingActions (apartmentId) {
    const query = Queries.getCalculatedSubjectLastClosingActions(apartmentId)
    const results = await this.db.query(query)
    const lastSoldAction = results.rows.find(action => action.status === 'sold')
    const lastRentedAction = results.rows.find(action => action.status === 'rented')
    return { lastSoldAction, lastRentedAction }
  }

  /**
   * Get the subject's most updated similar subject
   * from the same subject's Building
   * 
   * @param {number} beds 
   * @param {number} baths 
   * @param {number} apartmentId 
   * @param {number} buildingId 
   * @param {string} line 
   * @returns 
   * @memberof SubjectService
   */
  async getCalculatedSubjectLastEqualBedSubject (beds: number, baths: number, apartmentId: number, buildingId: number, line: string) { 
    let query = Queries.getCalculatedSubjectLastEqualSubjectSameLine(beds, baths, apartmentId, buildingId, line)
    let results = await this.db.query(query)
    if (results && results.rows.length) {
      return results.rows.pop();
    } else {
      query = Queries.getCalculatedSubjectLastEqualSubjectDifferentLine(beds, baths, apartmentId, buildingId, line)
      results = await this.db.query(query)
      if (results && results.rows.length) {
        return results.rows.pop()
      } else {
        return null
      }
    }
  }

  /**
   * 
   * 
   * @param {string} entityId 
   * @param {any} params 
   * @memberof SubjectService
   */
  async generate (entityId: string, params) {
    
    // Ref to all relavant services.
    const mediaService = this.parent.container.mediaService
    const compService = this.parent.container.compService
    const indexService = this.parent.container.indexService
    
    // Upper beds & baths.
    const beds = params.new_beds
    const baths = params.new_baths
    
    // Fetch the raw subject.
    let rawSubject = await this.getRawSubjectByEntityId(entityId)
    rawSubject = Helpers.castToNumber(rawSubject).pop()
    
    // Fetch its media and attach to object.
    rawSubject.media = await mediaService.getByCycleId(rawSubject.cycle_id)
    
    // -- Steps to set raw subject's Context object.
    //----------------------------------------------

    // Fetch raw subjects' upper beds and baths region comps.
    let comps = await compService.getComps(rawSubject.region, beds, baths)
    comps = Helpers.castToNumber(comps)
    
    let { region, neighborhood, building } = CompService.splitCompsToRgNbBl(comps, rawSubject.neighborhoods_group, rawSubject.building_id)
    
    // Set the context object.
    const context = new Context(region, neighborhood, building, beds, baths)

    // -- Steps to set raw subject's Index object.
    // -------------------------------------------
    
    // TODO: fetch the region indexes using the upper beds & baths as filters.
    let indexes = await indexService.getIndexes(rawSubject.region/*, beds, baths*/)
    indexes = Helpers.castToNumber(indexes)
    const filteredIndexes = IndexService.filterByUpperBedsBathsIndexes(indexes, beds, baths)
    const results = IndexService.splitCompsToRgNbBl(filteredIndexes, rawSubject.neighborhoods_group, rawSubject.building_id)
    const index = new Index(rawSubject, results.region, results.neighborhood, results.building, beds, baths)
    
    // Put everything in the subjectTransformer and invoke the 'transform' process.
    const subjectTransformer = this.parent.getSubjectTransformer(rawSubject, context, index, beds, baths, params)
    const subject = await subjectTransformer.transform()

    // Attach the comps to the subject object after preparing them 
    // for the client to be consumed. 
    // we change all properties to camel case and do some runtime calculations.
    subject.comps = CompService.beautify({ region, neighborhood, building })
    return SubjectDTO.map(subject)
  }

  async clear () {
    const query: String = Queries.clear()
    await this.db.query(query)
  }
}
