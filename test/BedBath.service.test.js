const { expect } = require('chai');
const _ = require('lodash');
const BedBathService = require('../src/services/BedBath.service');
const fixtures = require('./BedBathService.fixtures.json');

const groupByFixtures = (fixtures) => {
  let groupedFixtures = new Map();
  fixtures.forEach(element => {
    const {beds, baths, upper_beds, upper_baths} = element;
    if (!upper_beds || !upper_baths) return;
    
    const bedBath = `${beds},${baths}`;
    const upperBedBath = `${upper_beds},${upper_baths}`
    
    if (!groupedFixtures.has(bedBath)) {
      groupedFixtures.set(bedBath, [upperBedBath]);
    }
    else {
      groupedFixtures.get(bedBath).push(upperBedBath)
    }
  });
  return groupedFixtures;
}

describe ('BedBathService', () => {
  it('Returns the matching upper bed bath', () => {
    const bedBathService = new BedBathService();
    const groupedFixtures = groupByFixtures(fixtures);        
    [...groupedFixtures.entries()].forEach(([key, value]) => {
      const [bed, bath] = key.split(',');
      const bedsBathsList = bedBathService.getUpperBedsBaths(+bed, +bath);
      bedsBathsList.forEach(({beds: upperBeds, baths: upperBaths}) => {
        expect(value.includes(`${upperBeds},${upperBaths}`), `${bed}, ${bath} - ${upperBeds}, ${upperBaths}`).to.be.true;
      });
    });
  });
});
